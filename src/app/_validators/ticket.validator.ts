import { AppConfig } from './../_helpers/app-config-handle';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { Constants } from '../_helpers';

export class TicketValidator {

    static gameNumber = '';
    static bookNumber = '';
    static packNumber = '';
    static ticketNumber = '';
    static vrnNumber = '';
    static chkSum = '';

    static ticketLength(gameList: any, checkStatus: string): ValidatorFn {
        return (control: AbstractControl): { [key: string]: boolean } | null => {
            if (control.value !== undefined && !this.searchGameNumber(gameList, control.value, checkStatus)){
                return { 'ticketlength': true};
            }       
            return null;
        };
    }

    static searchGameNumber(gameList: any, value: string, checkStatus: string) {
        if(typeof gameList.data != 'undefined'){
            let gamedata = gameList.data.games;
            if(value.match(/^([0-9]+(-)*[0-9]+)+$/)){
                let inputArr = value.split('-');
                let tempTicketLength = inputArr[0].length + ((typeof inputArr[1] != 'undefined') ? inputArr[1].length : 0) + ((typeof inputArr[2] != 'undefined') ? inputArr[2].length : 0);
                if(tempTicketLength <= AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].maxTicketLength){
                    for (let i = 0; i < gamedata.length; i++) {
                        let game = gamedata[i];
                        if (game.gameNumber == inputArr[0]) {
                            this.gameNumber = game.gameNumber;
                            if(inputArr[1] === undefined){
                                this.packNumber = '';   
                                this.bookNumber = '';                 
                                this.ticketNumber = '';
                                this.vrnNumber = '';
                                this.chkSum = '';
                                return true;
                            }else{                              
                                if(AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].packNumberEnable && inputArr[1].length >= game.packNumberDigits){
                                    this.packNumber = inputArr[1].substr(0, game.packNumberDigits);
                                }else{
                                    this.packNumber = '';
                                }
                                if((!this.packNumber.match(/^[0]+$/) && inputArr[1].length == game.packNumberDigits + game.bookNumberDigits) || (!AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].packNumberEnable && inputArr[1].length == game.bookNumberDigits)){
                                    if(!this.packNumber.match(/^[0]+$/) && inputArr[1].length == game.packNumberDigits + game.bookNumberDigits){
                                        this.bookNumber = inputArr[1].substr(game.packNumberDigits, game.bookNumberDigits);
                                    }
                                    if(!AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].packNumberEnable && inputArr[1].length == game.bookNumberDigits){
                                        this.bookNumber = inputArr[1].substr(0, game.bookNumberDigits);
                                    }  
                                    if(!this.bookNumber.match(/^[0]+$/)){
                                        if(inputArr[2] === undefined || checkStatus == 'book'){
                                            this.ticketNumber = '';
                                            this.vrnNumber = '';
                                            this.chkSum = '';
                                            return true;
                                        }else{
                                            if(inputArr[2].length >= game.ticketNumberDigits && checkStatus != 'book'){
                                                this.ticketNumber = inputArr[2].substr(0, game.ticketNumberDigits);
                                                if(!this.ticketNumber.match(/^[0]+$/)){
                                                    if(checkStatus == 'ticket' && inputArr[2].length == game.ticketNumberDigits){
                                                        this.vrnNumber = '';
                                                        this.chkSum = '';
                                                        return true;
                                                    }
                                                    if(checkStatus == 'vrn' && inputArr[2].length > game.ticketNumberDigits){
                                                        this.vrnNumber = inputArr[2].substr(game.ticketNumberDigits, AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].vrnLength);
                                                        if(!this.vrnNumber.match(/^[0]+$/)){
                                                            if(AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].checkSumEnable){
                                                                this.chkSum = inputArr[2].substr(game.ticketNumberDigits + AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].vrnLength, AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].checkSumLength);
                                                                if(inputArr[2].length == game.ticketNumberDigits + AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].vrnLength + AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].checkSumLength){
                                                                    return true;
                                                                } 
                                                            }else{
                                                                this.chkSum = '';
                                                                if(inputArr[2].length == game.ticketNumberDigits + AppConfig.CLIENT_CONFIG[Constants.CLIENT_CODE].vrnLength){
                                                                    return true;
                                                                }                                                                
                                                            }                                                            
                                                        }
                                                    }                                                         
                                                }                                                                       
                                            }
                                        }
                                    }                            
                                }                                                         
                            }
                        }
                    }
                }                
            }
        }
        return false;        
    }
}