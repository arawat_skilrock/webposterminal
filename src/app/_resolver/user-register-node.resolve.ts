import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { AuthenticationService } from '../_services';

@Injectable()
export class UserRegisterResolve implements Resolve<any>{
    constructor(private authenticationService: AuthenticationService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        let tempVar = <any>route;
        let params = JSON.parse(JSON.stringify(tempVar.params));
        if (!(typeof params.playerid != 'undefined' && typeof params.playername != 'undefined' &&
            typeof params.sessid != 'undefined' && typeof params.bal != 'undefined' &&
            typeof params.lang != 'undefined' && typeof params.curr != 'undefined' &&
            typeof params.alias != 'undefined' && typeof params.isMobileApp != 'undefined' &&
            typeof params.currDisplay != 'undefined')) {
            let tempUser = this.authenticationService.getUserInfoReturn();
            params.playerid = tempUser.playerid;
            params.playername = tempUser.playername;
            params.sessid = tempUser.sessid;
            params.bal = tempUser.bal;
            params.lang = tempUser.lang;
            params.curr = tempUser.curr;
            params.alias = tempUser.alias;
            params.isMobileApp = tempUser.isMobileApp;
            params.currDisplay = tempUser.currDisplay;
        }
        return this.authenticationService.setUserInfo(params);
    }

}  