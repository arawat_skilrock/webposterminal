﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../_models';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${environment[environment.client].domain}/users`);
    }

    getCurrent() {
      return this.http.get<User[]>( `${environment[environment.client].domain}/users`);
    }

    getById(id: number) {
        return this.http.get(`${environment[environment.client].domain}/users/` + id);
    }

    register(user: User) {
        return this.http.post(`${environment[environment.client].domain}/users/register`, user);
    }

    update(user: User) {
        return this.http.put(`${environment[environment.client].domain}/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment[environment.client].domain}/users/` + id);
    }
}
