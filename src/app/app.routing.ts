﻿import { Routes, RouterModule } from '@angular/router';
// import { ModuleWithProviders } from '@angular/core';
import { AuthGuard, AuthGuardLogin, AuthGuardEnable } from './_guards';
import { Constants } from './_helpers';

const appRoutes: Routes = [
    { path: 'dge', loadChildren: () => import('./draw/draw.module').then(m => m.DrawModule), canActivate: [AuthGuardEnable], data: { engine: ['dge'] } },
    // otherwise redirect to home
    { path: '**', redirectTo: Constants.CLIENT_GAMES.default }
];

export const routing = RouterModule.forRoot(appRoutes);
// export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: false, onSameUrlNavigation: 'reload' });