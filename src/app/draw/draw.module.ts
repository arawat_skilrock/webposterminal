﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AlertService, ResultService, AuthenticationService, UserService, DrawService, ToastService } from '../_services';
import { routingDraws } from './draw.routing';
import { HttpErrorInterceptor } from '../_interceptors';
import { TranslateModule } from "@ngx-translate/core";
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DrawComponent } from './draw.component';
import { SimilarModule } from "../_common_component/similar.module";
import { ViewticketComponent } from "./viewticket";
import { PurchaseConfirmPanelComponent } from './purchaseconfirm';
import { DrawmachineComponent } from "./drawmachine";
import { ResultPanelComponent } from './resultpanel';
import { NgxPaginationModule } from 'ngx-pagination';
import { CurrencySymbolPipe, CustomCurrencyPipe } from '../_directives';
import { EuroMillionComponent } from './euroMillion';
import { PurchasePanel3Component } from './purchasepanel3';
import { UserRegisterResolve } from '../_resolver';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    routingDraws,
    CommonModule,
    TranslateModule,
    NgxBarcodeModule,
    NgbModule,
    SimilarModule,
    NgxPaginationModule
  ],
  declarations: [
    DrawComponent,
    EuroMillionComponent,
    PurchasePanel3Component,
    PurchaseConfirmPanelComponent,
    ResultPanelComponent,
    ViewticketComponent,
    DrawmachineComponent,
  ],
  providers: [
    UserRegisterResolve,
    AlertService,
    ResultService,
    AuthenticationService,
    UserService,
    DrawService,
    CurrencySymbolPipe,
    CustomCurrencyPipe,
    //   {
    //     provide: HTTP_INTERCEPTORS,
    //     useClass: HttpErrorInterceptor,
    //     multi: true
    // },
    ToastService
  ]
})

export class DrawModule { }