import { Constants } from './../../_helpers/constants-handle';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from "@angular/common";
import { DrawService } from '../../_services/draw.service';
import { Router, ActivationEnd } from '@angular/router';
import { AuthenticationService, CommonService } from 'src/app/_services';
import { delay, filter, first } from 'rxjs/operators';

@Component({
  selector: '.myTicketwrapper',
  templateUrl: './viewticket.component.html',
})
export class ViewticketComponent implements OnInit, OnDestroy {

  gameData: any;
  gameDataSubscription: any;
  routeSubscription: any;
  ticketNumber: any;
  txnId: any;
  ticketResponse: any;
  showTicket: boolean;
  gameCode = '';
  gameId;
  numberArray = {};
  betData = {};
  gameName;
  userInfo: any;
  userInfoLocalSubscription: any;
  panelType: any;
  lastDrawResult = [];
  authenticationSubscription: any;
  trackTicketSubscription: any;
  backButtonStatus = true;
  filterDrawId = 0;
  clientCode = Constants.CLIENT_CODE;
  iconMapping = [];
  numberArray2 = {};

  constructor(
    private drawService: DrawService,
    private commonService: CommonService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private _location: Location
  ) {
    this.routeSubscription = router.events.pipe(
      filter(e => e instanceof ActivationEnd),
      first()
    ).subscribe(val => {
      let tempVar = <any>val;
      let params = tempVar.snapshot.params;
      this.ticketNumber = params.ticketno;
      this.txnId = params.transactionId;
      this.gameName = "";
      this.showTicket = true;
      if (typeof tempVar.snapshot.fragment !== 'undefined' && tempVar.snapshot.fragment !== null) {
        this.filterDrawId = parseInt(tempVar.snapshot.fragment);
      }
      if (typeof params.playerid != 'undefined' && typeof params.playername != 'undefined' &&
        typeof params.sessid != 'undefined' && typeof params.bal != 'undefined' &&
        typeof params.lang != 'undefined' && typeof params.curr != 'undefined' &&
        typeof params.alias != 'undefined' && typeof params.isMobileApp != 'undefined') {
        this.backButtonStatus = false;
      } else {
        this.backButtonStatus = true;
      }
      this.drawService.getTrackTicket(this.ticketNumber, this.txnId);
      this.commonService.iframe_resize();
    });
  }

  ngOnInit() {
    this.commonService.addEngineClass();
    this.userInfoLocalSubscription = this.authenticationService.subscribeuserInfoLocal().subscribe((response) => {
      this.userInfo = response;
      this.commonService.iframe_resize();
    });
    this.gameDataSubscription = this.drawService.subscribeGameData().pipe(
      delay(0),
      filter(response => response != null)).subscribe((response: any) => {
        response.data.responseData.gameRespVOs.forEach(element => {
          if (element.gameCode == this.gameCode) {
            this.gameData = element;
            this.gameId = element.id;
            this.numberArray = {};
            this.betData = {};
            if (this.gameCode == Constants.ONE_BY_TWELVE) {
              this.iconMapping = Constants[this.gameCode + '_ICON_MAPPING'];
            }
            for (let i = 0; i < element.numberConfig.range[0].ball.length; i++) {
              this.numberArray[element.numberConfig.range[0].ball[i].number] = element.numberConfig.range[0].ball[i];
            }
            if (this.gameCode == Constants.POWERBALL) {
              for (let i = 0; i < element.numberConfig.range[1].ball.length; i++) {
                this.numberArray2[element.numberConfig.range[1].ball[i].number] = element.numberConfig.range[1].ball[i];
              }
            }
            for (let i = 0; i < element.betRespVOs.length; i++) {
              this.betData[element.betRespVOs[i].betCode] = JSON.parse(JSON.stringify(element.betRespVOs[i]));
              let tempVar = {};
              for (let j = 0; j < this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType.length; j++) {
                tempVar[element.betRespVOs[i].pickTypeData.pickType[j].code] = this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType[j];
              }
              this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType = tempVar;
            }
          }
        });
        this.commonService.iframe_resize();
      });

    this.trackTicketSubscription = this.drawService.subscribeTrackTicket().pipe(
      filter(response => response != null)).subscribe(response => {
        if (response.data != 'undefined' && response.data.responseCode == 0) {
          this.ticketResponse = response.data.responseData;
          this.showTicket = true;
          this.gameCode = response.data.responseData.gameCode;
          this.gameName = response.data.responseData.gameName;
          this.panelType = Constants.GAME_DATA[Constants.DRAW_ENGINE][this.gameCode].panelType;
          this.ticketNumber = this.ticketResponse.tktNumber;
          this.drawService.getGameData(Constants.DRAW_ENGINE, this.gameCode);
          let tempDrawWinList = [];
          response.data.responseData.drawWinList.forEach(element => {
            if (this.filterDrawId == 0 || this.filterDrawId == element.drawId) {
              let panelMgt = [];
              let betAmountPaid = 0;
              let winningAmt = 0;
              element.drawDateTime = this.drawService.changeDateFormat(element.drawDateTime);
              element.panelWinList.forEach(item => {
                if (this.gameCode != Constants.POWERBALL) {
                  item.pickedData = item.pickedData.replace(/\[|\]| /g, '').split(",").filter((item) => {
                    return item != '-1';
                  });
                } else {
                  let tempArray = item.pickedData.replace(/\[|\]| /g, '').split("#");
                  for (let x = 0; x < tempArray.length; x++) {
                    tempArray[x] = tempArray[x].split(",").filter((item) => {
                      return item != '-1';
                    });
                  }
                  item.pickedData = tempArray;
                }
                if (typeof panelMgt[item.panelId - 1] == 'undefined') {
                  panelMgt[item.panelId - 1] = item;
                } else {
                  panelMgt[item.panelId - 1].lineAmountNativeCurr += item.lineAmountNativeCurr;
                  panelMgt[item.panelId - 1].lineAmountTicketCurr += item.lineAmountTicketCurr;
                  panelMgt[item.panelId - 1].winningAmt += item.winningAmt;
                  panelMgt[item.panelId - 1].promoWinAmt += item.promoWinAmt;
                  if (this.gameCode != Constants.POWERBALL) {
                    panelMgt[item.panelId - 1].pickedData = panelMgt[item.panelId - 1].pickedData.concat(item.pickedData).filter((value, index, self) => {
                      return self.indexOf(value) === index;
                    });
                  }
                }
                betAmountPaid += item.lineAmountTicketCurr;
                winningAmt += item.winningAmt;
              });
              panelMgt = panelMgt.filter((el) => el != null);
              panelMgt.forEach(item => {
                if (item.pickType.startsWith('Banker')) {
                  let tempArray = item.pickedData.slice();
                  tempArray.splice(0, 1);
                  item.pickedData = [[item.pickedData[0]], tempArray];
                }
              });
              element.panelWinList = panelMgt;
              element.betAmountPaid = betAmountPaid;
              element.winningAmt = winningAmt;
              if (this.panelType == 2 && element.winResult != 'RESULT AWAITED') {
                let temp = element.winResult.split(',');
                this.lastDrawResult = [
                  temp[0].split('#').join(''),
                  temp[1].split('#').join(''),
                  temp[2].split('#').join(''),
                  temp[3].split('#').join(''),
                  temp[4].split('#').join(''),
                  temp[5].split('#').join(''),
                  temp[6].split('#').filter(function (el) {
                    return el != -1;
                  }).join(''),
                ];
              }
              tempDrawWinList.push(element);
            }
          });
          response.data.responseData.drawWinList = tempDrawWinList;
        }
      });
    this.authenticationService.getUserInfoLocal();
  }

  ngOnDestroy(): void {
    if (typeof this.userInfoLocalSubscription != 'undefined') {
      this.userInfoLocalSubscription.unsubscribe();
    }
    if (typeof this.gameDataSubscription != 'undefined') {
      this.gameDataSubscription.unsubscribe();
    }
    if (typeof this.routeSubscription != 'undefined') {
      this.routeSubscription.unsubscribe();
    }
    if (typeof this.trackTicketSubscription != 'undefined') {
      this.trackTicketSubscription.unsubscribe();
    }
    if (typeof this.authenticationSubscription != 'undefined') {
      this.authenticationSubscription.unsubscribe();
    }
    this.commonService.iframe_resize();
  }

  back() {
    this._location.back();
  }

  checkAcHOMain(num, draws) {
    let arrayString = draws.winResult;
    if (draws.drawStatusForticket == 'CLAIM_ALLOW' && arrayString != 'RESULT AWAITED') {
      if (arrayString.includes(num)) {
        return 'active';
      } else {
        return 'hollow';
      }
    }
    return 'hollow';
  }

  checkAcHOPowerball(num, draws, index) {
    let arrayString = draws.winResult.split('#');
    if (draws.drawStatusForticket == 'CLAIM_ALLOW' && arrayString != 'RESULT AWAITED') {
      if (arrayString[index].includes(num)) {
        return 'active';
      } else {
        return 'hollow';
      }
    }
    return 'hollow';
  }

  checkAcHOSide(playType, pickType, draws) {
    let arrayString = draws.winResult;
    if (draws.drawStatusForticket == 'CLAIM_ALLOW' && arrayString != 'RESULT AWAITED') {
      let newArray = arrayString.split(',').filter((element) => !isNaN(element));
      if (playType == 'LuckySixFirstBallEvenOdd' || playType == '5/90FirstBallOdd/Even' || playType == '12/24FirstBallEvenOdd') {
        if (pickType.includes('EVEN') || pickType.includes('Even')) {
          if (parseInt(newArray[0]) % 2 == 0) {
            return 'active';
          }
        }
        if (pickType.includes('ODD') || pickType.includes('Odd')) {
          if (parseInt(newArray[0]) % 2 != 0) {
            return 'active';
          }
        }
      }
      if (playType == 'LuckySixLastBallEvenOdd' || playType == '5/90LastBallOdd/Even' || playType == '12/24LastBallEvenOdd') {
        if (pickType.includes('EVEN') || pickType.includes('Even')) {
          if (parseInt(newArray[newArray.length - 1]) % 2 == 0) {
            return 'active';
          }
        }
        if (pickType.includes('ODD') || pickType.includes('Odd')) {
          if (parseInt(newArray[newArray.length - 1]) % 2 != 0) {
            return 'active';
          }
        }
      }
      if (playType == 'LuckySixFirstFiveMoreEvenOdd' || playType == 'MoreOddEven' || playType == '12/24MoreEvenOdd') {
        let count = {
          'odd': 0,
          'even': 0
        };
        let tempTotalBallEvaluation = 5;
        if (this.gameCode == Constants.TWELVE_BY_TWENTY_FOUR) {
          tempTotalBallEvaluation = 12;
        }
        for (let i = 0; i < tempTotalBallEvaluation; i++) {
          if (parseInt(newArray[i]) % 2 != 0) {
            count.odd += 1;
          }
          if (parseInt(newArray[i]) % 2 == 0) {
            count.even += 1;
          }
        }
        if (pickType.includes('EVEN') || pickType.includes('Even')) {
          if (count.even > count.odd) {
            return 'active';
          }
        }
        if (pickType.includes('ODD') || pickType.includes('Odd')) {
          if (count.even < count.odd) {
            return 'active';
          }
        }
      }
      if (playType == 'LuckySixFirstBallSum' || playType == '5/90FirstBallSum' || playType == '12/24FirstBallSum') {
        if (pickType.includes('Lesser')) {
          var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[2]);
          if (parseFloat(newArray[0]) < totSum) {
            return 'active';
          }
        }
        if (pickType.includes('Greater')) {
          var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[0]);
          if (parseFloat(newArray[0]) > totSum) {
            return 'active';
          }
        }
      }
      if (playType == 'LuckySixLastBallSum' || playType == '5/90LastBallSum' || playType == '12/24LastBallSum') {
        if (pickType.includes('Lesser')) {
          var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[2]);
          if (parseFloat(newArray[newArray.length - 1]) < totSum) {
            return 'active';
          }
        }
        if (pickType.includes('Greater')) {
          var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[0]);
          if (parseFloat(newArray[newArray.length - 1]) > totSum) {
            return 'active';
          }
        }
      }
      if (playType == 'LuckySixFirstFiveBallSum' || playType == 'FirstFiveBallSum') {
        if (pickType.includes('Lesser')) {
          var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[2]);
          let sum = 0;
          for (let i = 0; i < 5; i++) {
            sum += parseFloat(newArray[i]);
          }
          if (sum < totSum) {
            return 'active';
          }
        }
        if (pickType.includes('Greater')) {
          var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[0]);
          let sum = 0;
          for (let i = 0; i < 5; i++) {
            sum += parseFloat(newArray[i]);
          }
          if (sum > totSum) {
            return 'active';
          }
        }
      }
      if (playType == 'LuckySixFirstBallColor' || playType == 'FirstBallColor' || playType == '12/24FirstBallColor') {
        var color = this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[1];
        if (this.numberArray[newArray[0]].color == color) {
          return 'active';
        }
      }
      if (playType == 'LuckySixLastBallColor' || playType == 'LastBallColor' || playType == '12/24LastBallColor') {
        var color = this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[1];
        if (this.numberArray[newArray[newArray.length - 1]].color == color) {
          return 'active';
        }
      }
      if (playType == 'Increasing/Decreasing') {
        if (pickType.includes('Increasing')) {
          let tempNum = 0;
          for (let i = 0; i < newArray.length; i++) {
            if (tempNum < newArray[i]) {
              tempNum = newArray[i];
            } else {
              return 'hollow';
            }
          }
          return 'active';
        }
        if (pickType.includes('Decreasing')) {
          let tempNum = 1000;
          for (let i = 0; i < newArray.length; i++) {
            if (tempNum > newArray[i]) {
              tempNum = newArray[i];
            } else {
              return 'hollow';
            }
          }
          return 'active';
        }
      }
      if (playType == 'Hill/Valley') {
        if (pickType.includes('Hill')) {
          let tempNum = 0;
          for (let i = 0; i < newArray.length; i++) {
            if (i < Math.ceil(newArray.length / 2)) {
              if (tempNum < newArray[i]) {
                tempNum = newArray[i];
              } else {
                return 'hollow';
              }
            } else {
              if (tempNum > newArray[i]) {
                tempNum = newArray[i];
              } else {
                return 'hollow';
              }
            }
          }
          return 'active';
        }
        if (pickType.includes('Valley')) {
          let tempNum = 1000;
          for (let i = 0; i < newArray.length; i++) {
            if (i < Math.ceil(newArray.length / 2)) {
              if (tempNum > newArray[i]) {
                tempNum = newArray[i];
              } else {
                return 'hollow';
              }
            } else {
              if (tempNum < newArray[i]) {
                tempNum = newArray[i];
              } else {
                return 'hollow';
              }
            }
          }
          return 'active';
        }
      }
    }
    return 'hollow';
  }

  getLastDrawResult(i) {
    if (typeof this.lastDrawResult != 'undefined') {
      return this.lastDrawResult[i];
    } else {
      return '';
    }
  }

  getBetName(betDispName) {
    if (betDispName != null) {
      let tempBetDispNameArray = betDispName.split('-');
      return tempBetDispNameArray[tempBetDispNameArray.length - 1];
    }
    return '';
  }

  loadBack() {
    if (this.drawService.fromMyTickets) {
      this.router.navigate(['/dge/my-tickets']);
    } else {
      this.commonService.redirectParent('loadBackUrl');
    }
  }

  fact(n) {
    let res = 1;
    for (let i = 2; i <= n; i++)
      res = res * i;
    return res;
    this.commonService.iframe_resize();
  }

  numberOfLines(drawIndex, panelIndex, gameCode) {
    if (gameCode == Constants.THAI_LOTTERY || gameCode == Constants.LOTTERY_DIAMOND) {
      return 1;
    } else {
      if (this.ticketResponse.drawWinList[drawIndex].panelWinList[panelIndex].pickType == 'Banker') {
        return (this.ticketResponse.drawWinList[drawIndex].panelWinList[panelIndex].pickedData[0].length * this.ticketResponse.drawWinList[drawIndex].panelWinList[panelIndex].pickedData[1].length);
      } else if (this.ticketResponse.drawWinList[drawIndex].panelWinList[panelIndex].pickType == 'Banker1AgainstAll') {
        return (this.gameData.numberConfig.range[0].ball.length - 1);
      } else if (/(Perm|perm)/.test(this.ticketResponse.drawWinList[drawIndex].panelWinList[panelIndex].pickType)) {
        let n = this.ticketResponse.drawWinList[drawIndex].panelWinList[panelIndex].pickedData.length;
        let r = parseInt(this.betData[this.ticketResponse.drawWinList[drawIndex].panelWinList[panelIndex].playType].pickTypeData.pickType[this.ticketResponse.drawWinList[drawIndex].panelWinList[panelIndex].pickType].range[0].pickCount.split(',')[0]) - 1;
        return (this.fact(n) / (this.fact(r) * this.fact(n - r)));
      } else {
        return 1;
      }
    }
  }

  buyAgain() {
    let ticketData = {
      "currencyCode": (typeof this.gameData != 'undefined' && typeof this.gameData.currencyRespVOs != 'undefined') ? this.gameData.currencyRespVOs[0].currencyCode : '',
      "drawData": [],
      "gameCode": this.ticketResponse.gameCode,
      "gameId": this.ticketResponse.gameId,
      "isAdvancePlay": false,
      "lastTicketNumber": 0,
      "noOfDraws": this.ticketResponse.drawWinList.length,
      "noOfPanel": this.ticketResponse.drawWinList[0].panelWinList.length,
      "panelData": [],
      "extra": {
        'engine': Constants.DRAW_ENGINE,
        'path': '/dge/' + this.ticketResponse.gameCode.toLowerCase().replace(/[ /]/g, '-')
      },
    }
    // for (let i = 0; i < this.ticketResponse.drawWinList.length; i++) {
    for (let i = 0; i < 1; i++) {
      // if (this.panelType == 1) {
      //   ticketData.drawData.push({
      //     "drawId": "" + this.ticketResponse.drawWinList[i].drawId,
      //   });
      //   ticketData.isAdvancePlay = true;
      // }
      for (let j = 0; j < this.ticketResponse.drawWinList[i].panelWinList.length; j++) {
        let pickConfig = this.betData[this.ticketResponse.drawWinList[i].panelWinList[j].playType].pickTypeData.pickType[this.ticketResponse.drawWinList[i].panelWinList[j].pickType].range[0].pickConfig;
        let betAmountMultiple = this.ticketResponse.drawWinList[i].panelWinList[j].lineAmountTicketCurr / (this.betData[this.ticketResponse.drawWinList[i].panelWinList[j].playType].unitPrice * this.numberOfLines(i, j, this.gameCode));
        if (this.panelType == 2) {
          ticketData.panelData.push({
            "betType": this.ticketResponse.drawWinList[i].panelWinList[j].playType,
            "pickType": this.ticketResponse.drawWinList[i].panelWinList[j].pickType,
            "pickConfig": pickConfig,
            "betAmountMultiple": betAmountMultiple,
            "quickPick": false,
            "pickedValues": [
              this.ticketResponse.drawWinList[i].panelWinList[j].pickedData.join(''),
            ],
            "qpPreGenerated": false,
            "totalNumbers": this.ticketResponse.drawWinList[i].panelWinList[j].pickedData.length,
          });
        } else if (this.panelType == 1) {
          let pickedValues = this.betData[this.ticketResponse.drawWinList[i].panelWinList[j].playType].winMode == 'MAIN' ? this.ticketResponse.drawWinList[i].panelWinList[j].pickedData : this.ticketResponse.drawWinList[i].panelWinList[j].pickedData[0];
          ticketData.panelData.push({
            "betType": this.ticketResponse.drawWinList[i].panelWinList[j].playType,
            "pickType": this.ticketResponse.drawWinList[i].panelWinList[j].pickType,
            "pickConfig": pickConfig,
            "betAmountMultiple": betAmountMultiple,
            "quickPick": false,
            "pickedValues": pickedValues,
            "qpPreGenerated": false,
          });
        } else if (this.panelType == 3) {
          let pickedValues = this.ticketResponse.drawWinList[i].panelWinList[j].pickedData[0];
          let tempIndex = this.findPanelIndex(ticketData.panelData, pickedValues);
          if (tempIndex) {
            ticketData.panelData[parseInt(tempIndex)].totalNumbers += 1;
          } else {
            ticketData.panelData.push({
              "betType": this.ticketResponse.drawWinList[i].panelWinList[j].playType,
              "pickType": this.ticketResponse.drawWinList[i].panelWinList[j].pickType,
              "pickConfig": pickConfig,
              "betAmountMultiple": betAmountMultiple,
              "quickPick": false,
              "pickedValues": pickedValues,
              "qpPreGenerated": false,
              "totalNumbers": 1,
            });
          }
          ticketData.noOfPanel = ticketData.panelData.length;
        }
      }
    }
    this.commonService.setUserPurchaseDataPlayAgain(ticketData);
    if (!this.backButtonStatus) {
      this.commonService.redirectParent('loadDGE');
    } else {
      this.router.navigate([ticketData.extra.path]);
    }
  }

  findPanelIndex(panelData, num) {
    let index = 0;
    for (let i = 0; i < panelData.length; i++) {
      if (panelData[i].pickedValues == num && panelData[i].totalNumbers > 0) {
        index = i;
        return "" + index;
      }
    }
    return false;
  }

  drawWinNumber(winResult) {
    if (typeof winResult != undefined) {
      return winResult.split(',').filter((item) => !isNaN(parseInt(item)));
    }
    return [];
  }

  drawWinNumber2(winResult) {
    if (typeof winResult != undefined) {
      let tempResult = winResult.split('#');
      for (let i = 0; i < tempResult.length; i++) {
        tempResult[i] = tempResult[i].split(',').filter((item) => !isNaN(parseInt(item)));
      }
      return tempResult;
    }
    return [[], []];
  }

  checkDrawAddons(winningMultiplierInfo) {
    let status = false;
    // if (typeof winResult != 'undefined') {
    //   let arrTemp = winResult.split(',');
    //   for (let i = 0; i < arrTemp.length; i++) {
    //     status = arrTemp[i].includes(multString);
    //     if (status) {
    //       break;
    //     }
    //   }
    // }
    if (typeof winningMultiplierInfo != 'undefined' && winningMultiplierInfo != null && winningMultiplierInfo.multiplierCode.toLowerCase() != 'clear') {
      status = true;
    }
    return status;
  }

  checkClover(draws, num) {
    let status = false;
    // if (typeof winResult != 'undefined') {
    //   // let arrTemp = winResult.split(',').concat(["FLAG_MULTIPLIER_clover_23_47","FLAG_MULTIPLIER_clover2_7_18","DRAW_MULTIPLIER_Gold_-1_-1"]);
    //   let arrTemp = winResult.split(',');
    //   for (let i = 0; i < arrTemp.length; i++) {
    //     status = arrTemp[i].includes('FLAG_MULTIPLIER_clover');
    //     if (status && arrTemp[i].slice(-2) != num) {
    //       status = false;
    //     }
    //     if (status) {
    //       break;
    //     }
    //   }
    // }
    let runTimeFlagInfo = draws.runTimeFlagInfo;
    if (draws.drawStatusForticket == 'CLAIM_ALLOW' && typeof runTimeFlagInfo != 'undefined') {
      for (let i = 0; i < runTimeFlagInfo.length; i++) {
        if (parseInt(num) == runTimeFlagInfo[i].ballValue) {
          return true;
          break;
        }
      }
    }
    return status;
  }

  checkAcHOMainThai(panel, draws) {
    let status = 'hollow';
    if (draws.drawStatusForticket == 'CLAIM_ALLOW' && this.lastDrawResult.length != 0) {
      let betTypeArr = panel.playType.split('-');
      if (betTypeArr[0] == '3D') {
        if (betTypeArr[1] == 'Cat1' && betTypeArr[2] == 'Exact3') {
          if (this.lastDrawResult[1] == panel.pickedData.join('')) {
            status = 'active';
          }
        }
        if (betTypeArr[1] == 'Cat1' && betTypeArr[2] == 'Any3') {
          let permuteArr = this.permute(panel.pickedData.join(''), 0, 2);
          for (let i = 0; i < permuteArr.length; i++) {
            if (this.lastDrawResult[1] == permuteArr[i]) {
              status = 'active';
              break;
            }
          }
        }
        if (betTypeArr[1] == 'Cat2' && betTypeArr[2] == 'Exact3') {
          if (this.lastDrawResult[2] == panel.pickedData.join('') || this.lastDrawResult[3] == panel.pickedData.join('')) {
            status = 'active';
          }
        }
        if (betTypeArr[1] == 'Cat3' && betTypeArr[2] == 'Exact3') {
          if (this.lastDrawResult[4] == panel.pickedData.join('') || this.lastDrawResult[5] == panel.pickedData.join('')) {
            status = 'active';
          }
        }
      }
      if (betTypeArr[0] == '2D') {
        if (betTypeArr[1] == 'Cat1' && betTypeArr[2] == 'Exact2') {
          if (this.lastDrawResult[1].slice(-2) == panel.pickedData.join('')) {
            status = 'active';
          }
        }
        if (betTypeArr[1] == 'Cat4' && betTypeArr[2] == 'Exact2') {
          if (this.lastDrawResult[6] == panel.pickedData.join('')) {
            status = 'active';
          }
        }
      }
      if (betTypeArr[0] == '1D') {
        if (betTypeArr[1] == 'Cat1' && betTypeArr[2] == 'Any1') {
          if (this.lastDrawResult[1].includes(panel.pickedData.join(''))) {
            status = 'active';
          }
        }
        if (betTypeArr[1] == 'Cat4' && betTypeArr[2] == 'Any1') {
          if (this.lastDrawResult[6].includes(panel.pickedData.join(''))) {
            status = 'active';
          }
        }
      }
    }
    return status;
  }

  permute(str, l, r) {
    let tempArr = [];
    if (l == r) {
      return str;
    } else {
      for (let i = l; i <= r; i++) {
        str = this.swap(str, l, i);
        let a = this.permute(str, l + 1, r);
        if (Array.isArray(a)) {
          for (let t = 0; t < a.length; t++) {
            if (tempArr.indexOf(a[t]) < 0) {
              tempArr.push(a[t]);
            }
          }
        } else {
          if (tempArr.indexOf(a) < 0) {
            tempArr.push(a);
          }
        }
        str = this.swap(str, l, i);
      }
    }
    return tempArr;
  }

  swap(str, i, j) {
    let temp;
    let charArray = str.split('');
    temp = charArray[i];
    charArray[i] = charArray[j];
    charArray[j] = temp;
    return charArray.join('');
  }

  getPickName(code, name, isQuickPick) {
    if (name) {
      if (['HillPattern', 'Banker1 Against All'].indexOf(name) < 0) {
        name = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
      }
      if (/^(>|<)[\s]?[0-9]*\.?[0-9]+$/.test(name)) {
        name = name.replace(new RegExp(' ', 'g'), '');
      }
      if (!isQuickPick) {
        return name;
      }
      if (/(Perm|perm)/.test(code)) {
        return name + ' QP';
      } else {
        return 'QP'
      }
    }
    return null;
  }

  getSideBetPickName(name) {
    if (name) {
      if (name == 'a>b>c>d>e') {
        return 'Decreasing';
      } else if (name == 'a<b<c<d<e') {
        return 'Increasing';
      } else if (name == 'a<b<c>d>e') {
        return 'HillPattern';
      } else if (name == 'a>b>c<d<e') {
        return 'Valley';
      } else {
        name = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
        return name;
      }
    }
    return null;
  }

}