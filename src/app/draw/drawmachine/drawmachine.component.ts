import { DomSanitizer } from '@angular/platform-browser';
import { Constants } from './../../_helpers/constants-handle';
import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { DrawService } from '../../_services/draw.service';
import { AlertService } from '../../_services/alert.service';
import { ActivatedRoute, ActivationEnd, Router } from '@angular/router';
import { AuthenticationService, CommonService } from 'src/app/_services';
import { delay, filter, first } from "rxjs/operators";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: '.myTicketwrapper',
  templateUrl: './drawmachine.component.html',
})

export class DrawmachineComponent implements OnInit, OnDestroy {

  @HostListener('window:message', ['$event']) onMessage(e) {
    this.receiveMessage(e);
  }

  drawNumber: any;
  drawDateTime: any;
  showTickets: any;
  myTickets: any;
  iframe_src: any;
  showResult = false;
  screenName = "";
  panelType: any;
  lastDrawResult = [];
  gameData: any;
  gameCode = '';
  gameId;
  gameLink = "/dge/lobby";
  numberArray = {};
  drawData = {};
  betData = {};
  playerLogin = false;
  userInfo: any;
  clientCode = Constants.CLIENT_CODE;
  authenticationSubscription: any;
  userInfoLocalSubscription: any;
  gameDataSubscription: any;
  routeSubscription: any;
  trackTicketSubscription: any;
  playerTicketDataSubscription: any;
  iconMapping = [];
  numberArray2 = {};

  constructor(
    private drawService: DrawService,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private sanitizer: DomSanitizer,
    private authenticationService: AuthenticationService,
    private router: Router,
    private translate: TranslateService,
    private alertService: AlertService,
  ) {
    this.routeSubscription = router.events.pipe(
      filter(e => e instanceof ActivationEnd),
      first()
    ).subscribe(val => {
      let tempVar = <any>val;
      let params = tempVar.snapshot.params;
      if (typeof params.playerid != 'undefined' && typeof params.playername != 'undefined' &&
        typeof params.sessid != 'undefined' && typeof params.bal != 'undefined' &&
        typeof params.lang != 'undefined' && typeof params.curr != 'undefined' &&
        typeof params.alias != 'undefined' && typeof params.isMobileApp != 'undefined') {
        if (typeof params.sessid !== "undefined" && params.sessid !== '-') {
          this.playerLogin = true;
        }
      }
      if (typeof tempVar.snapshot.fragment !== 'undefined' && tempVar.snapshot.fragment !== null) {
        let fragment: string = route.snapshot.fragment;
        if (fragment != '') {
          this.gameCode = fragment;
          this.gameLink = '/dge/' + this.gameCode.toLowerCase().replace(/[ /]/g, '-');
        } else {
          this.gameLink = "/dge/lobby";
        }
      }
      this.commonService.iframe_resize();
    });
  }

  ngOnInit() {
    this.commonService.addEngineClass();

    this.userInfoLocalSubscription = this.authenticationService.subscribeuserInfoLocal().subscribe((response) => {
      this.userInfo = response;
      this.iframe_src = this.sanitizer.bypassSecurityTrustResourceUrl(Constants.GAME_DATA[Constants.DRAW_ENGINE][this.gameCode].draw_machine_url + `?currencyCode=${this.userInfo.curr}&currencyCodeDisplay=${this.userInfo.currDisplay}`);
      if (typeof this.userInfo.sessid !== "undefined" && this.userInfo.sessid !== '-') {
        this.playerLogin = true;
      }
      this.commonService.iframe_resize();
    });

    this.gameDataSubscription = this.drawService.subscribeGameData().pipe(
      delay(0),
      filter(response => response != null)).subscribe((response: any) => {
        response.data.responseData.gameRespVOs.forEach(element => {
          if (element.gameCode == this.gameCode) {
            this.gameData = element;
            this.gameId = element.id;
            this.drawData = {};
            this.numberArray = {};
            this.betData = {};
            if (this.gameCode == Constants.ONE_BY_TWELVE) {
              this.iconMapping = Constants[this.gameCode + '_ICON_MAPPING'];
            }
            for (let i = 0; i < element.drawRespVOs.length; i++) {
              if (element.drawRespVOs[i].drawStatus == 'ACTIVE') {
                this.drawData[' ' + element.drawRespVOs[i].drawId] = this.drawService.changeDateFormat(element.drawRespVOs[i].drawDateTime);
              }
            }
            for (let i = 0; i < element.numberConfig.range[0].ball.length; i++) {
              this.numberArray[element.numberConfig.range[0].ball[i].number] = element.numberConfig.range[0].ball[i];
            }
            if (this.gameCode == Constants.POWERBALL) {
              for (let i = 0; i < element.numberConfig.range[1].ball.length; i++) {
                this.numberArray2[element.numberConfig.range[1].ball[i].number] = element.numberConfig.range[1].ball[i];
              }
            }
            for (let i = 0; i < element.betRespVOs.length; i++) {
              this.betData[element.betRespVOs[i].betCode] = JSON.parse(JSON.stringify(element.betRespVOs[i]));
              let tempVar = {};
              for (let j = 0; j < this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType.length; j++) {
                tempVar[element.betRespVOs[i].pickTypeData.pickType[j].code] = this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType[j];
              }
              this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType = tempVar;
            }
          }
        });
        this.commonService.iframe_resize();
      });

    this.authenticationService.getUserInfoLocal();
    this.commonService.iframe_resize();
  }

  ngOnDestroy(): void {
    if (typeof this.gameDataSubscription != 'undefined') {
      this.gameDataSubscription.unsubscribe();
    }
    if (typeof this.routeSubscription != 'undefined') {
      this.routeSubscription.unsubscribe();
    }
    if (typeof this.authenticationSubscription != 'undefined') {
      this.authenticationSubscription.unsubscribe();
    }
    if (typeof this.userInfoLocalSubscription != 'undefined') {
      this.userInfoLocalSubscription.unsubscribe();
    }
    if (typeof this.trackTicketSubscription != 'undefined') {
      this.trackTicketSubscription.unsubscribe();
    }
    if (typeof this.playerTicketDataSubscription != 'undefined') {
      this.playerTicketDataSubscription.unsubscribe();
    }
    this.playerLogin = false;
    this.commonService.iframe_resize();
  }

  receiveMessage(event: any) {
    if (typeof event.data.event != "undefined" && event.data.event == "drawInfo") {
      let drawData = JSON.parse(event.data.data);
      // console.log("-----------------drawInfo------------------");
      // console.log(drawData);
      if (typeof drawData.cmd != "undefined" && drawData.cmd == "allDrawNum") {
        let winNum = drawData.draw;
        if (this.gameCode == Constants.THAI_LOTTERY || this.gameCode == Constants.LOTTERY_DIAMOND) {
          this.lastDrawResult = winNum.map(function (ele) {
            return ele.split('^').filter(function (el1) {
              return el1 != -1;
            }).join('');
          });
        } else {
          this.lastDrawResult = winNum;
        }
        this.screenName = "POST_DRAW";
        this.getMyTicketList(this.screenName);
      }
      if (typeof drawData.state != "undefined" && (drawData.state == "RESULT" || drawData.state == "FREEZE" || drawData.state == "NORMAL")) {
        this.drawNumber = drawData.content.drawId;
        this.drawDateTime = this.drawService.changeDateFormat(drawData.content.drawDateTime);
        if (this.gameCode == Constants.THAI_LOTTERY || this.gameCode == Constants.LOTTERY_DIAMOND) {
          this.lastDrawResult = drawData.sentResultData.map(function (ele) {
            return ele.split('^').filter(function (el1) {
              return el1 != -1;
            }).join('');
          });
        } else {
          this.lastDrawResult = drawData.sentResultData;
        }
        this.screenName = "RESULT_DRAW";
      } else if (typeof drawData.data != 'undefined' && drawData.data.screen_type == "IDLE") {
        this.drawNumber = drawData.data.content.GameDetailsList[0].drawId;
        this.drawDateTime = this.drawService.changeDateFormat(drawData.data.content.GameDetailsList[0].drawDateTime);
        this.screenName = "IDLE";
        this.lastDrawResult = [];
      } else if (typeof drawData.data != 'undefined' && drawData.data.screen_type == "PRE_DRAW") {
        this.drawNumber = drawData.data.current_draw_id;
        this.drawDateTime = this.drawService.changeDateFormat(drawData.data.content.GameDetailsList[0].drawDateTime);
        this.screenName = "PRE_DRAW";
        this.lastDrawResult = [];
      } else if (typeof drawData.data != 'undefined' && drawData.data.screen_type == 'POST_DRAW') {
        this.drawNumber = drawData.data.current_draw_id;
        this.drawDateTime = this.drawService.changeDateFormat(drawData.data.content.GameDetailsList[0].lastDrawWinningResult[0].lastDrawDateTime);
        let winNum = drawData.data.content.GameDetailsList[0].lastDrawWinningResult[0].winningNumber.split(",");
        if (this.gameCode == Constants.THAI_LOTTERY || this.gameCode == Constants.LOTTERY_DIAMOND) {
          this.lastDrawResult = winNum.map(function (ele) {
            return ele.split('^').filter(function (el1) {
              return el1 != -1;
            }).join('');
          });
        } else {
          this.lastDrawResult = winNum;
        }
        this.screenName = "POST_DRAW";
      }
      this.getMyTicketList(this.screenName);
    } else if (typeof event.data.event != 'undefined' && event.data.event == "result") {
      let drawData = JSON.parse(event.data.data);
      // console.log("-----------------result------------------");
      // console.log(drawData);
      if (drawData.cmd == "drawNum") {
        if (this.gameCode == Constants.THAI_LOTTERY || this.gameCode == Constants.LOTTERY_DIAMOND) {
          this.lastDrawResult.push(drawData.drawVal.split('^').filter(function (el1) {
            return el1 != -1;
          }).join(''));
        } else {
          this.lastDrawResult.push(drawData.drawVal);
        }
      }
    }
    this.commonService.iframe_resize();
  };

  getMyTicketList(screenType: any) {
    if (typeof this.drawNumber == "undefined" || (this.userInfo.sessid == '-' && this.userInfo.playerid == '-')) {
      return false;
    }
    let temp = {
      gameCode: this.gameCode,
      orderBy: 'asc',
      pageSize: 100,
      pageIndex: 0,
      drawId: this.drawNumber,
    };
    this.playerTicketDataSubscription = this.drawService.fetchPlayerTicketList(temp).pipe(
      filter(response => response != null)).subscribe((response: any) => {
        if (typeof response.data != 'undefined' && response.data.responseCode == 0) {
          this.myTickets = [];
          if (response.data.responseData.length > 0 && response.data.responseData[0].ticketList !== null && response.data.responseData[0].ticketList.length > 0) {
            this.myTickets = response.data.responseData[0].ticketList.reverse();
            this.drawService.getGameData(Constants.DRAW_ENGINE, this.gameCode);
            this.showTickets = true;
            if (typeof screenType != "undefined" && screenType == "POST_DRAW")
              this.showResult = true;
            else
              this.showResult = false;
            this.panelType = Constants.GAME_DATA[Constants.DRAW_ENGINE][this.gameCode].panelType;
            for (let i = 0; i < this.myTickets.length; i++) {
              let ticketNumber = this.myTickets[i].ticketNumber;
              this.getTrackTicket(ticketNumber, i);
            }
          } else {
            this.showTickets = false;
            this.commonService.iframe_resize();
          }
        } else {
          this.alertService.error(this.translate.instant(`error.dge.${response.responseCode}`));
        }
        this.commonService.iframe_resize();
      });
    this.commonService.iframe_resize();
  }

  getTrackTicket(ticketNumber, i) {
    this.trackTicketSubscription = this.drawService.trackTicket(ticketNumber, 0).subscribe(response => {
      if (response.data != 'undefined' && response.data.responseCode == 0) {
        let tempDrawWinList = [];
        response.data.responseData.drawWinList.forEach(element => {
          if (this.drawNumber == element.drawId) {
            let panelMgt = [];
            let betAmountPaid = 0;
            let winningAmt = 0;
            element.drawDateTime = this.drawService.changeDateFormat(element.drawDateTime);
            element.panelWinList.forEach(item => {
              if (this.gameCode != Constants.POWERBALL) {
                item.pickedData = item.pickedData.replace(/\[|\]| /g, '').split(",").filter((item) => {
                  return item != '-1';
                });
              } else {
                let tempArray = item.pickedData.replace(/\[|\]| /g, '').split("#");
                for (let x = 0; x < tempArray.length; x++) {
                  tempArray[x] = tempArray[x].split(",").filter((item) => {
                    return item != '-1';
                  });
                }
                item.pickedData = tempArray;
              }
              if (typeof panelMgt[item.panelId - 1] == 'undefined') {
                panelMgt[item.panelId - 1] = item;
              } else {
                panelMgt[item.panelId - 1].lineAmountNativeCurr += item.lineAmountNativeCurr;
                panelMgt[item.panelId - 1].lineAmountTicketCurr += item.lineAmountTicketCurr;
                panelMgt[item.panelId - 1].winningAmt += item.winningAmt;
                panelMgt[item.panelId - 1].promoWinAmt += item.promoWinAmt;
                if (this.gameCode != Constants.POWERBALL) {
                  panelMgt[item.panelId - 1].pickedData = panelMgt[item.panelId - 1].pickedData.concat(item.pickedData).filter((value, index, self) => {
                    return self.indexOf(value) === index;
                  });
                }
              }
              betAmountPaid += item.lineAmountTicketCurr;
              winningAmt += item.winningAmt;
            });
            panelMgt = panelMgt.filter((el) => el != null);
            panelMgt.forEach(item => {
              if (item.pickType.startsWith('Banker')) {
                let tempArray = item.pickedData.slice();
                tempArray.splice(0, 1);
                item.pickedData = [[item.pickedData[0]], tempArray];
              }
            });
            element.panelWinList = panelMgt;
            element.betAmountPaid = betAmountPaid;
            element.winningAmt = winningAmt;
            tempDrawWinList.push(element);
          }
        });
        response.data.responseData.drawWinList = tempDrawWinList;
        this.myTickets[i].ticketData = response.data.responseData;
      }
    });
  }

  checkDrawAddons() {
    let status = false;
    for (var i = 0; i < this.lastDrawResult.length; i++) {
      if (this.lastDrawResult[i].includes('DRAW_MULTIPLIER_TriFlag')) {
        return true;
        break;
      }
    }
    return status;
  }

  checkAcHOMain(num) {
    if (this.lastDrawResult.indexOf(num) > -1) {
      return 'aniSHOW active';
    }
    return 'hollow';
  }

  checkAcHOPowerball(num, index) {
    let tempResult = [[], []];
    let mark = false;
    for (let i = 0; i < this.lastDrawResult.length; i++) {
      let tempDrawNum = this.lastDrawResult[i].split('^');
      if (tempDrawNum.length > 1) {
        tempResult[0].push(tempDrawNum[0]);
        tempResult[1].push(tempDrawNum[1]);
        mark = true;
      } else if (mark) {
        tempResult[1].push(tempDrawNum[0]);
      } else {
        tempResult[0].push(tempDrawNum[0]);
      }
    }
    if (tempResult[index].indexOf(num) > -1) {
      return 'aniSHOW active';
    }
    return 'hollow';
  }

  checkClover(num) {
    let status = false;
    for (var i = 0; i < this.lastDrawResult.length; i++) {
      if (this.lastDrawResult[i].includes('FLAG_MULTIPLIER')) {
        let ball = this.lastDrawResult[i].split("_");
        ball = ball[ball.length - 1];
        if (parseInt(ball) == parseInt(num) && this.lastDrawResult.includes(ball)) {
          return true;
          break;
        }
      }
    }
    return status;
  }

  checkAcHOSide(playType, pickType) {
    let newArray = this.lastDrawResult.filter((element) => !isNaN(element));
    if ((playType == 'LuckySixFirstBallEvenOdd' || playType == '5/90FirstBallOdd/Even' || playType == '12/24FirstBallEvenOdd') && newArray.length > 0) {
      if (pickType.includes('EVEN') || pickType.includes('Even')) {
        if (parseInt(newArray[0]) % 2 == 0) {
          return 'active aniSHOW';
        }
      }
      if (pickType.includes('ODD') || pickType.includes('Odd')) {
        if (parseInt(newArray[0]) % 2 != 0) {
          return 'active aniSHOW';
        }
      }
    }
    if ((playType == 'LuckySixLastBallEvenOdd' && newArray.length == 35) || (playType == '5/90LastBallOdd/Even' && newArray.length == 5) || (playType == '12/24LastBallEvenOdd' && newArray.length == 12)) {
      if (pickType.includes('EVEN') || pickType.includes('Even')) {
        if (parseInt(newArray[newArray.length - 1]) % 2 == 0) {
          return 'active aniSHOW';
        }
      }
      if (pickType.includes('ODD') || pickType.includes('Odd')) {
        if (parseInt(newArray[newArray.length - 1]) % 2 != 0) {
          return 'active aniSHOW';
        }
      }
    }
    if (((playType == 'LuckySixFirstFiveMoreEvenOdd' || playType == 'MoreOddEven') && newArray.length > 4) || (playType == '12/24MoreEvenOdd' && newArray.length > 11)) {
      let count = {
        'odd': 0,
        'even': 0
      };
      let tempTotalBallEvaluation = 5;
      if (this.gameCode == Constants.TWELVE_BY_TWENTY_FOUR) {
        tempTotalBallEvaluation = 12;
      }
      for (let i = 0; i < tempTotalBallEvaluation; i++) {
        if (parseInt(newArray[i]) % 2 != 0) {
          count.odd += 1;
        }
        if (parseInt(newArray[i]) % 2 == 0) {
          count.even += 1;
        }
      }
      if (pickType.includes('EVEN') || pickType.includes('Even')) {
        if (count.even > count.odd) {
          return 'active aniSHOW';
        }
      }
      if (pickType.includes('ODD') || pickType.includes('Odd')) {
        if (count.even < count.odd) {
          return 'active aniSHOW';
        }
      }
    }
    if ((playType == 'LuckySixFirstBallSum' || playType == '5/90FirstBallSum' || playType == '12/24FirstBallSum') && newArray.length > 0) {
      if (pickType.includes('Lesser')) {
        var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[2]);
        if (parseFloat(newArray[0]) < totSum) {
          return 'active aniSHOW';
        }
      }
      if (pickType.includes('Greater')) {
        var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[0]);
        if (parseFloat(newArray[0]) > totSum) {
          return 'active aniSHOW';
        }
      }
    }
    if ((playType == 'LuckySixLastBallSum' && newArray.length == 35) || (playType == '5/90LastBallSum' && newArray.length == 5) || (playType == '12/24LastBallSum' && newArray.length == 12)) {
      if (pickType.includes('Lesser')) {
        var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[2]);
        if (parseFloat(newArray[newArray.length - 1]) < totSum) {
          return 'active aniSHOW';
        }
      }
      if (pickType.includes('Greater')) {
        var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[0]);
        if (parseFloat(newArray[newArray.length - 1]) > totSum) {
          return 'active aniSHOW';
        }
      }
    }
    if ((playType == 'LuckySixFirstFiveBallSum' || playType == 'FirstFiveBallSum') && newArray.length > 4) {
      if (pickType.includes('Lesser')) {
        var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[2]);
        let sum = 0;
        for (let i = 0; i < 5; i++) {
          sum += parseFloat(newArray[i]);
        }
        if (sum < totSum) {
          return 'active aniSHOW';
        }
      }
      if (pickType.includes('Greater')) {
        var totSum = parseFloat(this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[0]);
        let sum = 0;
        for (let i = 0; i < 5; i++) {
          sum += parseFloat(newArray[i]);
        }
        if (sum > totSum) {
          return 'active aniSHOW';
        }
      }
    }
    if ((playType == 'LuckySixFirstBallColor' || playType == 'FirstBallColor' || playType == '12/24FirstBallColor') && newArray.length > 0) {
      var color = this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[1];
      if (this.numberArray[newArray[0]].color == color) {
        return 'active aniSHOW';
      }
    }
    if ((playType == 'LuckySixLastBallColor' && newArray.length == 35) || (playType == 'LastBallColor' && newArray.length == 5) || (playType == '12/24LastBallColor' && newArray.length == 12)) {
      var color = this.betData[playType].pickTypeData.pickType[pickType].range[0].pickValue.split('<')[1];
      if (this.numberArray[newArray[newArray.length - 1]].color == color) {
        return 'active aniSHOW';
      }
    }
    if (playType == 'Increasing/Decreasing' && newArray.length > 4) {
      if (pickType.includes('Increasing')) {
        let tempNum = 0;
        for (let i = 0; i < newArray.length; i++) {
          if (tempNum < newArray[i]) {
            tempNum = newArray[i];
          } else {
            return 'hollow';
          }
        }
        return 'active aniSHOW';
      }
      if (pickType.includes('Decreasing')) {
        let tempNum = 1000;
        for (let i = 0; i < newArray.length; i++) {
          if (tempNum > newArray[i]) {
            tempNum = newArray[i];
          } else {
            return 'hollow';
          }
        }
        return 'active aniSHOW';
      }
    }
    if (playType == 'Hill/Valley' && newArray.length > 4) {
      if (pickType.includes('Hill')) {
        let tempNum = 0;
        for (let i = 0; i < newArray.length; i++) {
          if (i < Math.ceil(newArray.length / 2)) {
            if (tempNum < newArray[i]) {
              tempNum = newArray[i];
            } else {
              return 'hollow';
            }
          } else {
            if (tempNum > newArray[i]) {
              tempNum = newArray[i];
            } else {
              return 'hollow';
            }
          }
        }
        return 'active aniSHOW';
      }
      if (pickType.includes('Valley')) {
        let tempNum = 1000;
        for (let i = 0; i < newArray.length; i++) {
          if (i < Math.ceil(newArray.length / 2)) {
            if (tempNum > newArray[i]) {
              tempNum = newArray[i];
            } else {
              return 'hollow';
            }
          } else {
            if (tempNum < newArray[i]) {
              tempNum = newArray[i];
            } else {
              return 'hollow';
            }
          }
        }
        return 'active aniSHOW';
      }
    }
    return 'hollow';
  }

  checkAcHOMainThai(panel) {
    let status = 'hollow';
    let betTypeArr = panel.playType.split('-');
    if (betTypeArr[0] == '3D') {
      if (betTypeArr[1] == 'Cat1' && betTypeArr[2] == 'Exact3') {
        if (typeof this.lastDrawResult[1] !== 'undefined' && this.lastDrawResult[1] == panel.pickedData.join('')) {
          status = 'active aniSHOW';
        }
      }
      if (betTypeArr[1] == 'Cat1' && betTypeArr[2] == 'Any3') {
        let permuteArr = this.permute(panel.pickedData.join(''), 0, 2);
        for (let i = 0; i < permuteArr.length; i++) {
          if (typeof this.lastDrawResult[1] !== 'undefined' && this.lastDrawResult[1] == permuteArr[i]) {
            status = 'active aniSHOW';
            break;
          }
        }
      }
      if (betTypeArr[1] == 'Cat2' && betTypeArr[2] == 'Exact3') {
        if ((typeof this.lastDrawResult[2] !== 'undefined' && this.lastDrawResult[2] == panel.pickedData.join('')) || (typeof this.lastDrawResult[3] !== 'undefined' && this.lastDrawResult[3] == panel.pickedData.join(''))) {
          status = 'active aniSHOW';
        }
      }
      if (betTypeArr[1] == 'Cat3' && betTypeArr[2] == 'Exact3') {
        if ((typeof this.lastDrawResult[4] !== 'undefined' && this.lastDrawResult[4] == panel.pickedData.join('')) || (typeof this.lastDrawResult[5] !== 'undefined' && this.lastDrawResult[5] == panel.pickedData.join(''))) {
          status = 'active aniSHOW';
        }
      }
    }
    if (betTypeArr[0] == '2D') {
      if (betTypeArr[1] == 'Cat1' && betTypeArr[2] == 'Exact2') {
        if (typeof this.lastDrawResult[1] !== 'undefined' && this.lastDrawResult[1].slice(-2) == panel.pickedData.join('')) {
          status = 'active aniSHOW';
        }
      }
      if (betTypeArr[1] == 'Cat4' && betTypeArr[2] == 'Exact2') {
        if (typeof this.lastDrawResult[6] !== 'undefined' && this.lastDrawResult[6] == panel.pickedData.join('')) {
          status = 'active aniSHOW';
        }
      }
    }
    if (betTypeArr[0] == '1D') {
      if (betTypeArr[1] == 'Cat1' && betTypeArr[2] == 'Any1') {
        if (typeof this.lastDrawResult[1] !== 'undefined' && this.lastDrawResult[1].includes(panel.pickedData.join(''))) {
          status = 'active aniSHOW';
        }
      }
      if (betTypeArr[1] == 'Cat4' && betTypeArr[2] == 'Any1') {
        if (typeof this.lastDrawResult[6] !== 'undefined' && this.lastDrawResult[6].includes(panel.pickedData.join(''))) {
          status = 'active aniSHOW';
        }
      }
    }
    // }
    return status;
  }

  getBetName(betDispName) {
    if (betDispName != null) {
      let tempBetDispNameArray = betDispName.split('-');
      return tempBetDispNameArray[tempBetDispNameArray.length - 1];
    }
    return '';
  }

  permute(str, l, r) {
    let tempArr = [];
    if (l == r) {
      return str;
    } else {
      for (let i = l; i <= r; i++) {
        str = this.swap(str, l, i);
        let a = this.permute(str, l + 1, r);
        if (Array.isArray(a)) {
          for (let t = 0; t < a.length; t++) {
            if (tempArr.indexOf(a[t]) < 0) {
              tempArr.push(a[t]);
            }
          }
        } else {
          if (tempArr.indexOf(a) < 0) {
            tempArr.push(a);
          }
        }
        str = this.swap(str, l, i);
      }
    }
    return tempArr;
  }

  swap(str, i, j) {
    let temp;
    let charArray = str.split('');
    temp = charArray[i];
    charArray[i] = charArray[j];
    charArray[j] = temp;
    return charArray.join('');
  }

  fact(n) {
    let res = 1;
    for (let i = 2; i <= n; i++)
      res = res * i;
    return res;
    this.commonService.iframe_resize();
  }

  numberOfLines(ticketIndex, drawIndex, panelIndex, gameCode) {
    if (gameCode == Constants.THAI_LOTTERY || gameCode == Constants.LOTTERY_DIAMOND) {
      return 1;
    } else {
      if (this.myTickets[ticketIndex].ticketData.drawWinList[drawIndex].panelWinList[panelIndex].pickType == 'Banker') {
        return (this.myTickets[ticketIndex].ticketData.drawWinList[drawIndex].panelWinList[panelIndex].pickedData[0].length * this.myTickets[ticketIndex].ticketData.drawWinList[drawIndex].panelWinList[panelIndex].pickedData[1].length);
      } else if (this.myTickets[ticketIndex].ticketData.drawWinList[drawIndex].panelWinList[panelIndex].pickType == 'Banker1AgainstAll') {
        return (this.gameData.numberConfig.range[0].ball.length - 1);
      } else if (/(Perm|perm)/.test(this.myTickets[ticketIndex].ticketData.drawWinList[drawIndex].panelWinList[panelIndex].pickType)) {
        let n = this.myTickets[ticketIndex].ticketData.drawWinList[drawIndex].panelWinList[panelIndex].pickedData.length;
        let r = parseInt(this.betData[this.myTickets[ticketIndex].ticketData.drawWinList[drawIndex].panelWinList[panelIndex].playType].pickTypeData.pickType[this.myTickets[ticketIndex].ticketData.drawWinList[drawIndex].panelWinList[panelIndex].pickType].range[0].pickCount.split(',')[0]) - 1;
        return (this.fact(n) / (this.fact(r) * this.fact(n - r)));
      } else {
        return 1;
      }
    }
    this.commonService.iframe_resize();
  }

  getPickName(code, name, isQuickPick) {
    if (name) {
      if (['HillPattern', 'Banker1 Against All'].indexOf(name) < 0) {
        name = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
      }
      if (/^(>|<)[\s]?[0-9]*\.?[0-9]+$/.test(name)) {
        name = name.replace(new RegExp(' ', 'g'), '');
      }
      if (!isQuickPick) {
        return name;
      }
      if (/(Perm|perm)/.test(code)) {
        return name + ' QP';
      } else {
        return 'QP'
      }
    }
    return null;
  }

  getSideBetPickName(name) {
    if (name) {
      if (name == 'a>b>c>d>e') {
        return 'Decreasing';
      } else if (name == 'a<b<c<d<e') {
        return 'Increasing';
      } else if (name == 'a<b<c>d>e') {
        return 'HillPattern';
      } else if (name == 'a>b>c<d<e') {
        return 'Valley';
      } else {
        name = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
        return name;
      }
    }
    return null;
  }

  openLoginPopup() {
    this.commonService.openPlayerLogin();
  }

  loadBack() {
    this.commonService.removeParentHash();
    this.router.navigate([this.gameLink]);
  }

}