import { Constants } from './../../_helpers/constants-handle';
import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DrawService, AlertService, CommonService, AuthenticationService } from '../../_services';
import { Router, ActivationEnd } from '@angular/router';
import { filter, delay } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: '.confirmPurchaseOuterWrap',
  templateUrl: './purchase-confirm-panel.component.html',
})
export class PurchaseConfirmPanelComponent implements OnInit, OnDestroy {

  gameCode;
  gameId;
  gameData: any;
  userData: any;
  numberArray = {};
  numberArray2 = {};
  gameDataSubscription: any;
  drawData = {};
  betData = {};
  ticketPurchaseSubscription: any;
  numOfDrawsSelected: any;
  advanceDrawCount: string;
  advanceDrawErrorMsg = '';
  routeSubscription: any;
  advDrawPopupSubmitSubscription: any;
  ticketSubscription: any;
  userInfo: any;
  userInfoLocalSubscription: any;
  gameFetchCounter = 0;

  constructor(
    private drawService: DrawService,
    private alertService: AlertService,
    private commonService: CommonService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private renderer: Renderer2,
    private translate: TranslateService,
  ) {
    this.routeSubscription = router.events.pipe(
      filter(e => e instanceof ActivationEnd && typeof e.snapshot.data.gameCode != 'undefined'),
    ).subscribe(val => {
      this.gameCode = (<any>val).snapshot.data.gameCode;
      this.resetUserData();
      this.commonService.iframe_resize();
    });
  }

  ngOnInit() {
    this.gameDataSubscription = this.drawService.subscribeGameData().pipe(
      delay(0),
      filter(response => response != null && typeof response.data.responseData.gameRespVOs[0].numberConfig != 'undefined')).subscribe((response: any) => {
        response.data.responseData.gameRespVOs.forEach(element => {
          if (element.gameCode == this.gameCode) {
            this.gameData = element;
            this.gameId = element.id;
            this.drawData = {};
            this.numberArray = {};
            this.betData = {};
            for (let i = 0; i < element.drawRespVOs.length; i++) {
              if (element.drawRespVOs[i].drawStatus == 'ACTIVE') {
                this.drawData[' ' + element.drawRespVOs[i].drawId] = element.drawRespVOs[i].drawDateTime;
              }
            }
            for (let i = 0; i < element.numberConfig.range[0].ball.length; i++) {
              this.numberArray[element.numberConfig.range[0].ball[i].number] = element.numberConfig.range[0].ball[i];
            }
            if (this.gameCode == Constants.POWERBALL || this.gameCode == Constants.EURO_MILLION) {
              for (let i = 0; i < element.numberConfig.range[1].ball.length; i++) {
                this.numberArray2[element.numberConfig.range[1].ball[i].number] = element.numberConfig.range[1].ball[i];
              }
            }
            for (let i = 0; i < element.betRespVOs.length; i++) {
              this.betData[element.betRespVOs[i].betCode] = JSON.parse(JSON.stringify(element.betRespVOs[i]));
              let tempVar = {};
              for (let j = 0; j < this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType.length; j++) {
                tempVar[element.betRespVOs[i].pickTypeData.pickType[j].code] = this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType[j];
              }
              this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType = tempVar;
            }
            if (this.gameFetchCounter == 0) {
              if (response.timerType == this.gameCode || response.timerType == '') {
                this.resetUserData();
              }
            } else {
              this.checkAndResetDrawDate();
            }
            ++this.gameFetchCounter;
          }
        });
        this.commonService.iframe_resize();
      });

    this.advDrawPopupSubmitSubscription = this.drawService.subscribeAdvDrawPopupSubmit().subscribe((response: any) => {
      this.userData = response.userData;
      this.numOfDrawsSelected = '';
      this.advanceDrawCount = '' + this.userData.drawData.length;
      this.drawService.updateUserData(this.userData);
      this.commonService.iframe_resize();
    });

    this.ticketSubscription = this.drawService.subscribeTicketStatus().subscribe((response: any) => {
      this.userData = response.ticketData;
      ++this.gameFetchCounter;
      this.checkAndResetDrawDate();
      this.addConfirmPanel();
      this.commonService.iframe_resize();
    });

    this.userInfoLocalSubscription = this.authenticationService.subscribeuserInfoLocal().subscribe((response) => {
      this.userInfo = response;
      this.commonService.iframe_resize();
    });

    this.authenticationService.getUserInfoLocal();
    this.drawService.getGameData(Constants.DRAW_ENGINE, this.gameCode);

  }

  ngOnDestroy(): void {
    if (typeof this.gameDataSubscription != 'undefined') {
      this.gameDataSubscription.unsubscribe();
    }
    if (typeof this.routeSubscription != 'undefined') {
      this.routeSubscription.unsubscribe();
    }
    if (typeof this.ticketPurchaseSubscription != 'undefined') {
      this.ticketPurchaseSubscription.unsubscribe();
    }
    if (typeof this.advDrawPopupSubmitSubscription != 'undefined') {
      this.advDrawPopupSubmitSubscription.unsubscribe();
    }
    if (typeof this.ticketSubscription != 'undefined') {
      this.ticketSubscription.unsubscribe();
    }
    if (typeof this.userInfoLocalSubscription != 'undefined') {
      this.userInfoLocalSubscription.unsubscribe();
    }
    this.commonService.iframe_resize();
  }

  checkAndResetDrawDate() {
    if (this.userData.drawData.length > 0) {
      let resetStatus = false;
      for (let i = 0; i < this.userData.drawData.length; i++) {
        if (typeof this.drawData[' ' + this.userData.drawData[i].drawId] == 'undefined') {
          resetStatus = true;
          break;
        }
      }
      if (resetStatus == true) {
        this.userData.drawData = [];
        this.userData.isAdvancePlay = false;
        this.userData.noOfDraws = 1;
        this.numOfDrawsSelected = 1;
        this.advanceDrawCount = '0';
        this.alertService.info(this.translate.instant('dge.draw_reset_info'));
      } else {
        this.numOfDrawsSelected = '';
        this.advanceDrawCount = '' + this.userData.drawData.length;
      }
    } else {
      this.numOfDrawsSelected = '' + this.userData.noOfDraws;
      this.advanceDrawCount = '0';
    }
  }

  addConfirmPanel() {
    this.renderer.removeClass(document.getElementsByClassName('bodyWrapper')[0], 'vi-mainbet');
    this.renderer.removeClass(document.getElementsByClassName('bodyWrapper')[0], 'vi-sidebet');
    this.renderer.addClass(document.getElementsByClassName('bodyWrapper')[0], 'vi-confirm');
    this.commonService.iframe_resize();
  }

  resetUserData() {
    this.userData = {
      // "currencyCode": (typeof this.gameData != 'undefined' && typeof this.gameData.currencyRespVOs != 'undefined') ? this.gameData.currencyRespVOs[0].currencyCode : '',
      // "drawData": [{"drawId" : ""+this.gameData.drawRespVOs[0].drawId}],
      "drawData": [],
      "gameCode": this.gameCode,
      "gameId": (typeof this.gameData != 'undefined' && typeof this.gameData.id != 'undefined') ? this.gameId : 0,
      "isAdvancePlay": false,
      "lastTicketNumber": 0,
      // "noOfDraws": 1,
      "noOfDraws": 1,
      "noOfPanel": 0,
      "panelData": [],
    }
    this.numOfDrawsSelected = 1;
    this.advanceDrawCount = '0';
    this.advanceDrawErrorMsg = "";
    this.commonService.iframe_resize();
  }

  onDrawInputChange() {
    let regex = /[^0-9]/g;
    const initalValue = this.numOfDrawsSelected;
    this.numOfDrawsSelected = initalValue.replace(regex, '').substr(0, 2);
    this.userData.drawData = [];
    this.advanceDrawCount = '0';
    if (this.numOfDrawsSelected != '' && parseInt(this.numOfDrawsSelected) > 0 && parseInt(this.numOfDrawsSelected) <= this.gameData.maxAdvanceDraws) {
      if (parseInt(this.numOfDrawsSelected) <= this.gameData.maxAdvanceDraws) {
        this.userData.noOfDraws = parseInt(this.numOfDrawsSelected);
      } else {
        this.alertService.error(this.translate.instant('dge.cons_draw_err', { numOfDrawsSelected: this.numOfDrawsSelected }));
        this.numOfDrawsSelected = 1;
        this.userData.noOfDraws = 1;
      }
      //////////////////////////////////////////////////////////////
      this.userData.isAdvancePlay = false;
      //////////////////////////////////////////////////////////////
    } else if (this.numOfDrawsSelected == '') {
      this.userData.noOfDraws = 0;
      //////////////////////////////////////////////////////////////
      this.userData.isAdvancePlay = true;
      //////////////////////////////////////////////////////////////
    } else {
      this.alertService.error(this.translate.instant('dge.max_draw_err', { numOfDrawsSelected: this.numOfDrawsSelected }));
      this.numOfDrawsSelected = 1;
      this.userData.noOfDraws = 1;
      //////////////////////////////////////////////////////////////
      this.userData.isAdvancePlay = false;
      //////////////////////////////////////////////////////////////
    }
    this.drawService.updateUserData(this.userData);
    this.commonService.iframe_resize();
  }

  drawChange(sign) {
    this.advanceDrawCount = '0';
    if (this.numOfDrawsSelected == '') {
      this.numOfDrawsSelected = 1;
    } else {
      if (sign == '+') {
        if ((parseInt(this.numOfDrawsSelected) + 1) <= this.gameData.maxAdvanceDraws) {
          this.numOfDrawsSelected = '' + (parseInt(this.numOfDrawsSelected) + 1);
        }
      } else {
        if ((parseInt(this.numOfDrawsSelected) - 1) >= 1) {
          this.numOfDrawsSelected = '' + (parseInt(this.numOfDrawsSelected) - 1);
        }
      }
    }
    this.userData.noOfDraws = parseInt(this.numOfDrawsSelected);
    this.userData.drawData = [];
    this.userData.isAdvancePlay = false;
    this.drawService.updateUserData(this.userData);
    this.commonService.iframe_resize();
  }

  openAdvDrawPopup() {
    this.drawService.setAdvDrawPopupStatus(true, this.userData);
    this.commonService.iframe_resize();
  }

  numberOfLines(purchasePanelIndex) {
    if (this.userData.panelData[purchasePanelIndex].pickType == 'Banker') {
      return (this.userData.panelData[purchasePanelIndex].pickedValues[0].length * this.userData.panelData[purchasePanelIndex].pickedValues[1].length);
    } else if (this.userData.panelData[purchasePanelIndex].pickType == 'Banker1AgainstAll') {
      return (this.gameData.numberConfig.range[0].ball.length - 1);
    } else if (/(Perm|perm)/.test(this.userData.panelData[purchasePanelIndex].pickType)) {
      let n = this.userData.panelData[purchasePanelIndex].pickedValues.length;
      let r = parseInt(this.betData[this.userData.panelData[purchasePanelIndex].betType].pickTypeData.pickType[this.userData.panelData[purchasePanelIndex].pickType].range[0].pickCount.split(',')[0]) - 1;
      return (this.fact(n) / (this.fact(r) * this.fact(n - r)));
    } else {
      return 1;
    }
    this.commonService.iframe_resize();
  }

  fact(n) {
    let res = 1;
    for (let i = 2; i <= n; i++)
      res = res * i;
    return res;
    this.commonService.iframe_resize();
  }

  calculatePanelPrice(pIndex) {
    if (typeof this.userData != 'undefined') {
      let unitPrice = this.betData[this.userData.panelData[pIndex].betType].unitPrice;
      let betAmountMultiple = this.userData.panelData[pIndex].betAmountMultiple;
      return (unitPrice * betAmountMultiple * this.numberOfLines(pIndex) * this.userData.noOfDraws);
    }
    return 0;
    this.commonService.iframe_resize();
  }

  calculateTotalPrice() {
    if (typeof this.userData != 'undefined') {
      let totalSum = 0;
      for (let i = 0; i < this.userData.panelData.length; i++) {
        totalSum += this.calculatePanelPrice(i);
      }
      return totalSum;
    }
    return 0;
    this.commonService.iframe_resize();
  }

  setUserPurchaseData() {
    this.userData.extra = {
      'engine': Constants.DRAW_ENGINE,
      'path': '/dge/' + this.gameData.gameCode.toLowerCase().replace(/[ /]/g, '-')
    };
    this.commonService.setUserPurchaseData(this.userData);
  }

  confirmTicketPurchase() {
    if (this.userData.noOfDraws == 0) {
      this.alertService.error(this.translate.instant('dge.draw_select_err'));
      return false;
    }
    if (this.userInfo.sessid == '-' && this.userInfo.playerid == '-') {
      this.setUserPurchaseData();
      // this.alertService.error('Player not Logged In !');
      return false;
    }
    let ticketData = JSON.parse(JSON.stringify(this.userData));
    for (let i = 0; i < ticketData.panelData.length; i++) {
      if (ticketData.panelData[i].pickType == 'Banker') {
        ticketData.panelData[i].pickedValues = ticketData.panelData[i].pickedValues[0].join() + '-' + ticketData.panelData[i].pickedValues[1].join();
      } else if (ticketData.panelData[i].pickType == 'Banker1AgainstAll') {
        ticketData.panelData[i].pickedValues = ticketData.panelData[i].pickedValues[0].join();
      } else if (this.betData[ticketData.panelData[i].betType].winMode == 'MAIN') {
        if (this.gameCode != Constants.POWERBALL && this.gameCode != Constants.EURO_MILLION) {
          ticketData.panelData[i].pickedValues = ticketData.panelData[i].pickedValues.join();
        } else {
          ticketData.panelData[i].pickedValues = ticketData.panelData[i].pickedValues[0].join() + '#' + ticketData.panelData[i].pickedValues[1].join();
        }
      }
    }
    this.ticketPurchaseSubscription = this.drawService.ticketPurchase(ticketData).subscribe((response: any) => {
      if (typeof response.data != 'undefined' && response.data.responseCode == 0) {
        this.drawService.resetUserData(true);
        this.authenticationService.updatePlayerBalance(response.data.responseData.availableBal);
        this.closeTicket();
        this.resetUserData();
        this.drawService.updateTicketList(true);
        this.alertService.successBuy(this.translate.instant('dge.buy_succ'), '/dge/view-ticket/' + response.data.responseData.ticketNumber + '/0', response.data.responseData.ticketNumber);
      } else {
        if (response.responseCode == 203) {
          this.setUserPurchaseData();
        } else if (response.responseCode == 1019) {
          this.alertService.info(this.translate.instant(`error.dge.${response.responseCode}`));
        } else {
          this.alertService.error(this.translate.instant(`error.dge.${response.responseCode}`));
        }
      }
      this.commonService.iframe_resize();
    });
  }

  removePanel(pIndex) {
    this.userData.panelData.splice(pIndex, 1);
    this.userData.noOfPanel = this.userData.panelData.length;
    if (this.userData.noOfPanel == 0) {
      this.resetUserData();
      this.closeTicket();
    }
    this.drawService.updateUserData(this.userData);
    this.commonService.iframe_resize();
  }

  closeTicket() {
    if (window.innerWidth >= Constants.TAB_WIDTH) {
      this.renderer.addClass(document.getElementsByClassName('bodyWrapper')[0], 'vi-mainbet');
    } else {
      this.renderer.removeClass(document.getElementsByClassName('bodyWrapper')[0], 'vi-mainbet');
    }
    this.renderer.removeClass(document.getElementsByClassName('bodyWrapper')[0], 'vi-sidebet');
    this.renderer.removeClass(document.getElementsByClassName('bodyWrapper')[0], 'vi-confirm');
    this.drawService.resetPlayUserData(true);
    this.commonService.iframe_resize();
  }

  getPickName(code, name, isQuickPick) {
    if (name) {
      if (['HillPattern', 'Banker1 Against All'].indexOf(name) < 0) {
        name = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
      }
      if (/^(>|<)[\s]?[0-9]*\.?[0-9]+$/.test(name)) {
        name = name.replace(new RegExp(' ', 'g'), '');
      }
      if (!isQuickPick) {
        return name;
      }
      if (/(Perm|perm)/.test(code)) {
        return name + ' QP';
      } else {
        return 'QP'
      }
    }
    return null;
  }

  getSideBetPickName(name) {
    if (name) {
      if (name == 'a>b>c>d>e') {
        return 'Decreasing';
      } else if (name == 'a<b<c<d<e') {
        return 'Increasing';
      } else if (name == 'a<b<c>d>e') {
        return 'HillPattern';
      } else if (name == 'a>b>c<d<e') {
        return 'Valley';
      } else {
        name = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
        return name;
      }
    }
    return null;
  }

}