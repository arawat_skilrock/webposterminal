﻿import { Routes, RouterModule } from '@angular/router';
import { DrawComponent } from "./draw.component";
import { Constants } from '../_helpers';
import { ViewticketComponent } from "./viewticket";
import { DrawmachineComponent } from "./drawmachine";
import { EuroMillionComponent } from './euroMillion';
import { UserRegisterResolve } from '../_resolver';

const appRoutes: Routes = [
    {
        path: '',
        component: DrawComponent,
        children: [
            { path: ':playerid/:playername/:sessid/:bal/:lang/:curr/:currDisplay/:alias/:isMobileApp', component: EuroMillionComponent, data: { gameCode: Constants.EURO_MILLION }, resolve: { userData: UserRegisterResolve, } },
            { path: 'euromillion', component: EuroMillionComponent, data: { gameCode: Constants.EURO_MILLION } },
            { path: 'euromillion/:playerid/:playername/:sessid/:bal/:lang/:curr/:currDisplay/:alias/:isMobileApp', component: EuroMillionComponent, data: { gameCode: Constants.EURO_MILLION }, resolve: { userData: UserRegisterResolve, } },
            { path: 'view-ticket/:ticketno/:transactionId', component: ViewticketComponent },
            { path: 'view-ticket/:ticketno/:transactionId/:playerid/:playername/:sessid/:bal/:lang/:curr/:currDisplay/:alias/:isMobileApp', component: ViewticketComponent, resolve: { userData: UserRegisterResolve, } },
            { path: 'draw-machine', component: DrawmachineComponent },
            { path: 'draw-machine/:playerid/:playername/:sessid/:bal/:lang/:curr/:currDisplay/:alias/:isMobileApp', component: DrawmachineComponent, resolve: { userData: UserRegisterResolve, } },
            { path: '**', redirectTo: '-/-/-/0/en/USD/-/-/0' }
        ],
    },
];

// const appRoutes: Routes = [
//     { path: 'mini-keno', component: SleComponent, data: { gameCode: Constants.TWELVE_BY_TWENTY_FOUR } },
//     { path: 'super-keno', component: SleComponent, data: { gameCode: Constants.SUPER_KENO } },
//     { path: 'quick-10', component: SleComponent, data: { gameCode: Constants.TEN_BY_TWENTY } },
//     { path: 'keno-5-90', component: SleComponent, data: { gameCode: Constants.FIVE_BY_NINETY } },
//     { path: 'lucky-6', component: SleComponent, data: { gameCode: Constants.LUCKY_SIX } },
//     { path: 'full-roulette', component: SleComponent, data: { gameCode: Constants.FULL_ROULETTE } },
//     { path: '**', redirectTo: 'keno-5-90' },
// ];

export const routingDraws = RouterModule.forChild(appRoutes);
// export const routing: ModuleWithProviders = RouterModule.forChild(appRoutes);