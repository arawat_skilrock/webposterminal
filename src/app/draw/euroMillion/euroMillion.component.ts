import { Constants } from 'src/app/_helpers';
import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DrawService, AlertService, CommonService, AuthenticationService } from 'src/app/_services';
import { filter, delay, first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivationEnd } from '@angular/router';

@Component({
  selector: '.game-zone',
  templateUrl: './euroMillion.component.html',
})

export class EuroMillionComponent implements OnInit, OnDestroy {

  gameCode = Constants.EURO_MILLION;
  currentEnterNumIndex = 0;
  panelCounter = 1;
  panelNumberCounter;
  isFixedBet = false;
  gameData: any;
  userData: any;
  qpNumber;
  selectedQpNum = 0;
  gameDataSubscription: any;
  quickPickDataSubscription: any;
  rootData: any;
  betAmtArray = [];
  otherAmtVal = '';
  showOtherAmountInput = false;
  qpArray = [];
  isBankerBet = false;
  bankerUPNcounter;
  bankerLPNcounter;
  betData = {};
  otherBetAmountMultiple = 0;
  betError = '';
  userInfo: any;
  timeIntervalContainer: any;
  tempCounter = 0;
  fetchDataStatus = false;
  diff_time: any;
  resetPlaySubscription: any;
  postState = true;
  postStateMsg = '';
  userInfoLocalSubscription: any;
  notifyDrawMachineSubscription: any;
  postStateInterval: any;
  isQP = false;
  gameFetchCounter = 0;
  myTicketCount = 0;
  ticketListSubscription: any;
  notifyTktLstSubscription: any;
  clientCode = Constants.CLIENT_CODE;
  saleTimerStatus = false;
  panelNumberCounter2;
  qpArray2 = [];
  qpNumber2;
  selectedQpNum2 = 0;
  totalBetPanel = 0;
  totalPricePanel = 0;
  panelCounterSubscription: any;
  loadLobby = true;
  routeSubscription: any;
  authenticationSubscription: any;

  constructor(
    private drawService: DrawService,
    private alertService: AlertService,
    private renderer: Renderer2,
    private commonService: CommonService,
    private authenticationService: AuthenticationService,
    private translate: TranslateService,
    private router: Router,
  ) {
    this.rootData = Constants.GAME_DATA[Constants.DRAW_ENGINE][this.gameCode];
    this.routeSubscription = router.events.pipe(
      filter(e => e instanceof ActivationEnd),
      first()
    ).subscribe(val => {
      let tempVar = <any>val;
      let params = tempVar.snapshot.params;
      if (typeof params.playerid != 'undefined' && typeof params.playername != 'undefined' &&
        typeof params.sessid != 'undefined' && typeof params.bal != 'undefined' &&
        typeof params.lang != 'undefined' && typeof params.curr != 'undefined' &&
        typeof params.alias != 'undefined' && typeof params.isMobileApp != 'undefined') {
        this.loadLobby = false;
      }
      this.commonService.iframe_resize();
    });
  }

  ngOnInit() {
    this.commonService.addEngineClass();
    this.gameDataSubscription = this.drawService.subscribeGameData().pipe(
      delay(0),
      filter(response => response != null && typeof response.data.responseData.gameRespVOs[0].numberConfig != 'undefined'))
      .subscribe((response: any) => {
        response.data.responseData.gameRespVOs.forEach(element => {
          if (element.gameCode == this.gameCode) {
            this.gameData = JSON.parse(JSON.stringify(element));
            if (this.gameFetchCounter == 0) {
              if (response.timerType == this.gameCode || response.timerType == '') {
                this.resetUserData();
              }
            }
            this.betAmtArray = [];
            for (let i = 0; i < element.betRespVOs.length; i++) {
              this.betData[element.betRespVOs[i].betCode] = JSON.parse(JSON.stringify(element.betRespVOs[i]));
              let tempVar = {};
              let tempPickCounter = 0;
              for (let j = 0; j < element.betRespVOs[i].pickTypeData.pickType.length; j++) {
                if (element.betRespVOs[i].pickTypeData.pickType[j].range[0].qpAllowed == 'YES') {
                  this.gameData.betRespVOs[i].pickTypeData.pickType[j + tempPickCounter].range[0].qpAllowed = 'NO';
                  this.gameData.betRespVOs[i].pickTypeData.pickType.splice(j + 1, 0, JSON.parse(JSON.stringify(element.betRespVOs[i].pickTypeData.pickType[j])));
                  if (/(Perm|perm)/.test(this.gameData.betRespVOs[i].pickTypeData.pickType[j + tempPickCounter + 1].code)) {
                    this.gameData.betRespVOs[i].pickTypeData.pickType[j + tempPickCounter + 1].name += ' QP';
                  } else {
                    this.gameData.betRespVOs[i].pickTypeData.pickType[j + tempPickCounter + 1].name = 'QP';
                  }
                  this.gameData.betRespVOs[i].pickTypeData.pickType[j + tempPickCounter + 1].code += ' QP';
                  this.gameData.betRespVOs[i].pickTypeData.pickType[j + tempPickCounter + 1].range[0].qpAllowed = 'YES';
                  this.gameData.betRespVOs[i].pickTypeData.pickType[j + tempPickCounter + 1].range[0].pickMode = 'QP';
                  tempVar[element.betRespVOs[i].pickTypeData.pickType[j].code] = JSON.parse(JSON.stringify(this.gameData.betRespVOs[i].pickTypeData.pickType[j + tempPickCounter]));
                  tempVar[element.betRespVOs[i].pickTypeData.pickType[j].code + ' QP'] = JSON.parse(JSON.stringify(this.gameData.betRespVOs[i].pickTypeData.pickType[j + tempPickCounter + 1]));
                  ++tempPickCounter;
                } else {
                  tempVar[element.betRespVOs[i].pickTypeData.pickType[j].code] = this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType[j];
                }
              }
              this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType = tempVar;
            }
            let betArrayLength = Math.floor(element.betRespVOs[0].maxBetAmtMul);
            betArrayLength = (betArrayLength > 8) ? 9 : betArrayLength;
            if (element.betRespVOs[0].unitPrice <= 1) {
              for (let i = 1; i <= betArrayLength; i++) {
                if (element.betRespVOs[0].unitPrice * i * 10 <= element.betRespVOs[0].maxBetAmtMul * element.betRespVOs[0].unitPrice) {
                  this.betAmtArray.push(element.betRespVOs[0].unitPrice * i * 10);
                }
              }
            } else {
              for (let i = 1; i <= betArrayLength; i++) {
                if (element.betRespVOs[0].unitPrice * i <= element.betRespVOs[0].maxBetAmtMul * element.betRespVOs[0].unitPrice) {
                  this.betAmtArray.push(element.betRespVOs[0].unitPrice * i);
                }
              }
            }
            this.fetchDataStatus = true;
            this.tempCounter = 0;
            clearInterval(this.timeIntervalContainer);
            if (response.timerType == element.gameCode || response.timerType == "") {
              // let tempTime = Math.floor((new Date(element.timeToFetchUpdatedGameInfo.replace(/-/g, "/")).getTime() - new Date().getTime()) / 1000);
              let tempTime = Math.floor((new Date(this.drawService.changeDateFormat(element.timeToFetchUpdatedGameInfo)).getTime() - DrawService.gameTime) / 1000);
              let tempTime2 = 0;
              if (typeof element.drawRespVOs[0].drawSaleStartTime != 'undefined') {
                tempTime2 = Math.floor((new Date(this.drawService.changeDateFormat(element.drawRespVOs[0].drawSaleStartTime)).getTime() - DrawService.gameTime) / 1000);
              }
              this.diff_time = {
                totalTime: tempTime >= 0 ? tempTime : 0,
                shownTime: ['00', '00', '00', '00'],
                convention: 'sec',
                saleStartTime: tempTime2 >= 0 ? tempTime2 : 0,
                saleShownTime: ['00', '00', '00', '00'],
                saleConvention: 'sec',
              }
            }
            ++this.gameFetchCounter;
          }
        });
        // this.fetchPlayerTicketList();
        this.updateClock();
        this.timeIntervalContainer = setInterval(() => {
          this.updateClock();
        }, 1000);
        this.commonService.iframe_resize();
      });
    this.resetPlaySubscription = this.drawService.subscribeResetPlayUserData().subscribe((response) => {
      if (response.status) {
        this.resetUserData();
      }
      this.commonService.iframe_resize();
    });
    this.userInfoLocalSubscription = this.authenticationService.subscribeuserInfoLocal().subscribe((response) => {
      this.userInfo = response;
      this.commonService.iframe_resize();
    });
    // this.notifyTktLstSubscription = this.drawService.subscribeTicketList().subscribe((response) => {
    //   this.fetchPlayerTicketList();
    // });
    this.panelCounterSubscription = this.drawService.subscribePanelCounter().subscribe((response) => {
      this.totalBetPanel = response.panelCounter;
      this.totalPricePanel = response.totalPrice;
    });
  }

  ngOnDestroy(): void {
    this.removeBetPanels();
    this.commonService.closeAllModals();
    this.renderer.removeClass(document.body, 'modal-open');
    this.renderer.removeClass(document.body, 'modal-fix');
    clearInterval(this.timeIntervalContainer);
    clearInterval(this.postStateInterval);
    if (typeof this.gameDataSubscription != 'undefined') {
      this.gameDataSubscription.unsubscribe();
    }
    if (typeof this.quickPickDataSubscription != 'undefined') {
      this.quickPickDataSubscription.unsubscribe();
    }
    if (typeof this.resetPlaySubscription != 'undefined') {
      this.resetPlaySubscription.unsubscribe();
    }
    if (typeof this.userInfoLocalSubscription != 'undefined') {
      this.userInfoLocalSubscription.unsubscribe();
    }
    if (typeof this.notifyDrawMachineSubscription != 'undefined') {
      this.notifyDrawMachineSubscription.unsubscribe();
    }
    if (typeof this.ticketListSubscription != 'undefined') {
      this.ticketListSubscription.unsubscribe();
    }
    if (typeof this.notifyTktLstSubscription != 'undefined') {
      this.notifyTktLstSubscription.unsubscribe();
    }
    if (typeof this.panelCounterSubscription != 'undefined') {
      this.panelCounterSubscription.unsubscribe();
    }
    if (typeof this.routeSubscription != 'undefined') {
      this.routeSubscription.unsubscribe();
    }
    if (typeof this.authenticationSubscription != 'undefined') {
      this.authenticationSubscription.unsubscribe();
    }
    this.commonService.iframe_resize();
  }

  loadBackPanel() {
    this.removeBetPanels();
    this.resetUserData();
    this.commonService.iframe_resize();
  }

  removeBetPanels() {
    this.renderer.removeClass(document.getElementsByClassName('bodyWrapper')[0], 'vi-mainbet');
    this.renderer.removeClass(document.getElementsByClassName('bodyWrapper')[0], 'vi-sidebet');
    this.renderer.removeClass(document.getElementsByClassName('bodyWrapper')[0], 'vi-confirm');
    this.commonService.iframe_resize();
  }

  resetUserData() {
    this.userData = {
      "panelData": [
        {
          "betType": this.gameData.betRespVOs[0].betCode,
          "pickType": this.gameData.betRespVOs[0].pickTypeData.pickType[0].code,
          "pickConfig": this.gameData.betRespVOs[0].pickTypeData.pickType[0].range[0].pickConfig,
          "betAmountMultiple": (this.gameData.betRespVOs[0].unitPrice <= 1) ? (this.gameData.betRespVOs[0].unitPrice * 10) : 1,
          "quickPick": false,
          "pickedValues": [Array(+this.gameData.betRespVOs[0].pickTypeData.pickType[0].range[0].pickCount.split(',')[1]), Array(+this.gameData.betRespVOs[0].pickTypeData.pickType[0].range[1].pickCount.split(',')[1])],
          "qpPreGenerated": false
        },
      ],
    };
    this.panelNumberCounter = +this.gameData.betRespVOs[0].pickTypeData.pickType[0].range[0].pickCount.split(',')[1];
    this.panelNumberCounter2 = +this.gameData.betRespVOs[0].pickTypeData.pickType[0].range[1].pickCount.split(',')[1];
    this.qpNumber = +this.gameData.betRespVOs[0].pickTypeData.pickType[0].range[0].pickCount.split(',')[0];
    this.qpNumber2 = +this.gameData.betRespVOs[0].pickTypeData.pickType[0].range[1].pickCount.split(',')[0];
    this.qpArray = [];
    for (let i = this.qpNumber; i <= this.panelNumberCounter; i++) {
      this.qpArray.push(i);
    }
    this.qpArray2 = [];
    for (let i = this.qpNumber2; i <= this.panelNumberCounter2; i++) {
      this.qpArray2.push(i);
    }
    setTimeout(() => {
      this.postStateMsg = this.translate.instant('dge.' + this.gameCode + '_' + this.gameData.betRespVOs[0].betCode + '_' + this.gameData.betRespVOs[0].pickTypeData.pickType[0].code + '_desc');
    }, 500);
    this.selectedQpNum = 0;
    this.selectedQpNum2 = 0;
    this.otherAmtVal = '';
    this.showOtherAmountInput = false;
    this.panelCounter = 1;
    this.isFixedBet = false;
    this.isBankerBet = false;
    this.isQP = false;
    this.currentEnterNumIndex = 0;
    this.bankerUPNcounter = 0;
    this.bankerLPNcounter = 0;
    this.commonService.iframe_resize();
  }

  selectGameBet(betCode) {
    if (this.userData.panelData[this.panelCounter - 1].betType != betCode) {
      this.gameData.betRespVOs.forEach(element => {
        if (betCode == element.betCode) {
          this.userData.panelData[this.panelCounter - 1].betType = element.betCode;
          this.userData.panelData[this.panelCounter - 1].pickType = element.pickTypeData.pickType[0].code;
          this.userData.panelData[this.panelCounter - 1].pickConfig = element.pickTypeData.pickType[0].range[0].pickConfig;
          this.userData.panelData[this.panelCounter - 1].quickPick = false;
          this.userData.panelData[this.panelCounter - 1].qpPreGenerated = false;
          this.postStateMsg = this.translate.instant('dge.' + this.gameCode + '_' + betCode + '_' + element.pickTypeData.pickType[0].code + '_desc');
          this.isBankerBet = false;
          this.panelNumberCounter = +element.pickTypeData.pickType[0].range[0].pickCount.split(',')[1];
          this.panelNumberCounter2 = +element.pickTypeData.pickType[0].range[1].pickCount.split(',')[1];
          this.qpNumber = +element.pickTypeData.pickType[0].range[0].pickCount.split(',')[0];
          this.qpNumber2 = +element.pickTypeData.pickType[0].range[1].pickCount.split(',')[0];
          this.qpArray = [];
          this.qpArray2 = [];
          for (let i = this.qpNumber; i <= this.panelNumberCounter; i++) {
            this.qpArray.push(i);
          }
          for (let i = this.qpNumber2; i <= this.panelNumberCounter2; i++) {
            this.qpArray2.push(i);
          }
          this.bankerUPNcounter = 0;
          this.bankerLPNcounter = 0;
          if (element.pickTypeData.pickType[0].range[0].pickMode == 'QP') {
            this.userData.panelData[this.panelCounter - 1].pickedValues = [Array(this.panelNumberCounter), Array(this.panelNumberCounter2)];
            this.isFixedBet = false;
            this.isQP = (this.qpArray.length > 1) ? true : false;
            this.qpNumbers(this.qpNumber, 0);
            this.qpNumbers(this.qpNumber2, 1);
          } else {
            this.isFixedBet = false;
            this.isQP = false;
            this.userData.panelData[this.panelCounter - 1].pickedValues = [Array(this.panelNumberCounter), Array(this.panelNumberCounter2)];
          }
        }
      });
      this.currentEnterNumIndex = 0;
      if (betCode.endsWith(' QP')) {
        this.selectedQpNum = this.qpNumber;
        this.selectedQpNum2 = this.qpNumber2;
      } else {
        this.selectedQpNum = 0;
        this.selectedQpNum2 = 0;
      }
    }
    this.commonService.iframe_resize();
  }

  moveBetFocus(event, dir, index) {
    if (dir == 'RIGHT' && typeof event.srcElement.parentNode.children[index + 1] != 'undefined') {
      event.srcElement.parentNode.children[index + 1].focus();
    }
    if (dir == 'LEFT' && typeof event.srcElement.parentNode.children[index - 1] != 'undefined') {
      event.srcElement.parentNode.children[index - 1].focus();
    }
    this.commonService.iframe_resize();
  }

  selectGamePick(betCode, index) {
    if (this.userData.panelData[this.panelCounter - 1].pickType != betCode || (this.userData.panelData[this.panelCounter - 1].pickType == betCode && betCode.endsWith(' QP')) || (this.userData.panelData[this.panelCounter - 1].pickType == betCode && betCode.startsWith('Banker'))) {
      this.gameData.betRespVOs[index].pickTypeData.pickType.forEach(element => {
        if (betCode == element.code) {
          this.userData.panelData[this.panelCounter - 1].pickType = element.code;
          this.userData.panelData[this.panelCounter - 1].pickConfig = element.range[0].pickConfig;
          this.userData.panelData[this.panelCounter - 1].quickPick = false;
          this.userData.panelData[this.panelCounter - 1].qpPreGenerated = false;
          this.postStateMsg = this.translate.instant('dge.' + this.gameCode + '_' + this.gameData.betRespVOs[index].betCode + '_' + element.code + '_desc');
          this.isBankerBet = false;
          this.panelNumberCounter = +element.range[0].pickCount.split(',')[1];
          this.panelNumberCounter2 = +element.range[1].pickCount.split(',')[1];
          this.qpNumber = +element.range[0].pickCount.split(',')[0];
          this.qpNumber2 = +element.range[1].pickCount.split(',')[0];
          this.qpArray = [];
          this.qpArray2 = [];
          this.bankerUPNcounter = 0;
          this.bankerLPNcounter = 0;
          for (let i = this.qpNumber; i <= this.panelNumberCounter; i++) {
            this.qpArray.push(i);
          }
          for (let i = this.qpNumber2; i <= this.panelNumberCounter2; i++) {
            this.qpArray2.push(i);
          }
          if (element.range[0].pickMode == 'QP') {
            this.userData.panelData[this.panelCounter - 1].pickedValues = [Array(this.panelNumberCounter), Array(this.panelNumberCounter2)];
            this.isFixedBet = false;
            this.isQP = (this.qpArray.length > 1) ? true : false;
            this.qpNumbers(this.qpNumber, 0);
            this.qpNumbers(this.qpNumber2, 1);
          } else {
            this.isFixedBet = false;
            this.isQP = false;
            this.userData.panelData[this.panelCounter - 1].pickedValues = [Array(this.panelNumberCounter), Array(this.panelNumberCounter2)];
          }
        }
      });
      this.currentEnterNumIndex = 0;
      if (betCode.endsWith(' QP')) {
        this.selectedQpNum = this.qpNumber;
        this.selectedQpNum2 = this.qpNumber2;
      } else {
        this.selectedQpNum = 0;
        this.selectedQpNum2 = 0;
      }
    }
    this.commonService.iframe_resize();
  }

  movePickFocus(event, dir, index) {
    if (dir == 'RIGHT' && typeof event.srcElement.parentNode.children[index + 1] != 'undefined') {
      event.srcElement.parentNode.children[index + 1].focus();
    }
    if (dir == 'LEFT' && typeof event.srcElement.parentNode.children[index - 1] != 'undefined') {
      event.srcElement.parentNode.children[index - 1].focus();
    }
    this.commonService.iframe_resize();
  }

  selectPickedNum(num, index) {
    let fullStatus = true;
    if (!this.userData.panelData[this.panelCounter - 1].pickType.startsWith('Banker')) {
      if (this.isFixedBet == true || this.isQP == true || this.userData.panelData[0].pickType.endsWith(' QP')) {
        this.alertService.error(this.translate.instant('dge.bet_man_err'));
        return;
      }
      let numAlreadyIndex = this.userData.panelData[this.panelCounter - 1].pickedValues[index].indexOf(num);
      if (numAlreadyIndex > -1) {
        this.userData.panelData[this.panelCounter - 1].pickedValues[index][numAlreadyIndex] = undefined;
      } else {
        if (this.userData.panelData[this.panelCounter - 1].pickedValues[index][this.currentEnterNumIndex] == undefined || this.userData.panelData[this.panelCounter - 1].pickedValues[index][this.currentEnterNumIndex] == '') {
          this.userData.panelData[this.panelCounter - 1].pickedValues[index][this.currentEnterNumIndex] = num;
          // document.getElementById('powerPlay' + (this.currentEnterNumIndex + 1)).blur();
          fullStatus = false;
        } else {
          for (let i = 0; i < this.userData.panelData[this.panelCounter - 1].pickedValues[index].length; i++) {
            if (this.userData.panelData[this.panelCounter - 1].pickedValues[index][i] == undefined || this.userData.panelData[this.panelCounter - 1].pickedValues[index][i] == '') {
              this.userData.panelData[this.panelCounter - 1].pickedValues[index][i] = num;
              fullStatus = false;
              break;
            }
          }
        }
        if (fullStatus == true) {
          this.alertService.error(this.translate.instant('dge.bet_dual_num_err', { panelNumber: this.panelNumberCounter, panelNumber2: this.panelNumberCounter2 }));
        }
      }
    }
    this.selectedQpNum = 0;
    this.commonService.iframe_resize();
  }

  checkNumberSelected(num, index) {
    if ((this.userData.panelData[this.panelCounter - 1].pickedValues[index].indexOf(num) > -1) ||
      (typeof this.userData.panelData[this.panelCounter - 1].pickedValues[index][0] != 'undefined' && this.userData.panelData[this.panelCounter - 1].pickedValues[index][0].indexOf(num) > -1) ||
      (typeof this.userData.panelData[this.panelCounter - 1].pickedValues[index][1] != 'undefined' && this.userData.panelData[this.panelCounter - 1].pickedValues[index][1].indexOf(num) > -1)) {
      return true;
    }
    return false;
    this.commonService.iframe_resize();
  }

  qpNumbers(num, index) {
    if (index == 0) {
      this.selectedQpNum = num;
      if (this.isFixedBet == true || this.isBankerBet == true) {
        this.alertService.error(this.translate.instant('dge.bet_aut_err'));
        return;
      }
      let numArray = this.drawService.qpGenerator({ numberConfig: this.gameData.numberConfig.range[0].ball, numbersPicked: num, noOfLines: 1 }).map((num) => {
        if (num.length == 1)
          return '0' + num;
        else
          return num;
      });
      if (/(Perm|perm)/.test(this.userData.panelData[this.panelCounter - 1].pickType)) {
        let numArrLength = numArray.length;
        for (let i = 0; i < (this.betData[this.userData.panelData[this.panelCounter - 1].betType].pickTypeData.pickType[this.userData.panelData[this.panelCounter - 1].pickType].range[0].pickCount.split(',')[1] - numArrLength); i++) {
          numArray.push(null);
        }
      }
      this.userData.panelData[this.panelCounter - 1].pickedValues[index] = numArray;
    }
    if (index == 1) {
      this.selectedQpNum2 = num;
      if (this.isFixedBet == true || this.isBankerBet == true) {
        this.alertService.error(this.translate.instant('dge.bet_aut_err'));
        return;
      }
      let numArray = this.drawService.qpGenerator({ numberConfig: this.gameData.numberConfig.range[1].ball, numbersPicked: num, noOfLines: 1 }).map((num) => {
        if (num.length == 1)
          return '0' + num;
        else
          return num;
      });
      if (/(Perm|perm)/.test(this.userData.panelData[this.panelCounter - 1].pickType)) {
        let numArrLength = numArray.length;
        for (let i = 0; i < (this.betData[this.userData.panelData[this.panelCounter - 1].betType].pickTypeData.pickType[this.userData.panelData[this.panelCounter - 1].pickType].range[1].pickCount.split(',')[1] - numArrLength); i++) {
          numArray.push(null);
        }
      }
      this.userData.panelData[this.panelCounter - 1].pickedValues[index] = numArray;
    }
    this.userData.panelData[this.panelCounter - 1].quickPick = true;
    this.userData.panelData[this.panelCounter - 1].qpPreGenerated = true;
    this.commonService.iframe_resize();
  }

  closeQP() {
    this.isQP = false;
    this.commonService.iframe_resize();
  }

  closeBanker() {
    // let tempBankerUPNcounterMin = this.betData[this.userData.panelData[this.panelCounter - 1].betType].pickTypeData.pickType[this.userData.panelData[this.panelCounter - 1].pickType].range[0].pickCount.split('-')[0].split(',')[0];
    // let tempUPCounter = 0;
    // for (let i = 0; i < this.userData.panelData[this.panelCounter - 1].pickedValues[0].length; i++) {
    //   if (typeof this.userData.panelData[this.panelCounter - 1].pickedValues[0][i] != 'undefined') {
    //     tempUPCounter++;
    //   }
    // }
    // if (this.userData.panelData[this.panelCounter - 1].pickType == 'Banker') {
    //   let tempBankerLPNcounterMin = this.betData[this.userData.panelData[this.panelCounter - 1].betType].pickTypeData.pickType[this.userData.panelData[this.panelCounter - 1].pickType].range[0].pickCount.split('-')[1].split(',')[0];
    //   let tempLPCounter = 0;
    //   for (let i = 0; i < this.userData.panelData[this.panelCounter - 1].pickedValues[1].length; i++) {
    //     if (typeof this.userData.panelData[this.panelCounter - 1].pickedValues[1][i] != 'undefined') {
    //       tempLPCounter++;
    //     }
    //   }
    //   if (tempUPCounter < tempBankerUPNcounterMin || tempLPCounter < tempBankerLPNcounterMin) {
    //     this.alertService.error(this.translate.instant('dge.bet_num_banker_up_lw_err', { bankerUPNcounter: this.bankerUPNcounter, bankerLPNcounter: this.bankerLPNcounter }));
    //   } else {
    //     this.isBankerBet = false;
    //   }
    // } else {
    //   if (tempUPCounter < tempBankerUPNcounterMin) {
    //     this.alertService.error(this.translate.instant('dge.bet_num_banker_up_err', { bankerUPNcounter: this.bankerUPNcounter }));
    //   } else {
    //     this.isBankerBet = false;
    //   }
    // }
    this.isBankerBet = false;
    this.commonService.iframe_resize();
  }

  selectBetAmount(j, betAmt) {
    // event.stopPropagation();
    this.showOtherAmountInput = false;
    this.otherAmtVal = '';
    if (this.gameData.betRespVOs[0].unitPrice <= 1) {
      this.userData.panelData[this.panelCounter - 1].betAmountMultiple = (j + 1) * 10;
    } else {
      this.userData.panelData[this.panelCounter - 1].betAmountMultiple = (j + 1);
    }
    this.betError = '';
    this.commonService.iframe_resize();
  }

  /***********************For Other Bet Amt Modal Specific ***********************/

  // selectOtherAmount(event) {
  //   this.showOtherAmountInput = true;
  //   this.renderer.setElementClass(document.body, 'modal-open', true);
  //   this.betError = '';
  //   this.otherBetAmountMultiple = this.userData.panelData[this.panelCounter - 1].betAmountMultiple;
  //   if (this.betAmtArray.indexOf(this.userData.panelData[this.panelCounter - 1].betAmountMultiple) < 0) {
  //     this.otherAmtVal = '' + (this.userData.panelData[this.panelCounter - 1].betAmountMultiple * this.gameData.betRespVOs[0].unitPrice);
  //   } else {
  //     this.otherAmtVal = '';
  //   }
  //   setTimeout(() => {
  //     document.getElementById('otherAmt-' + this.panelCounter).focus();
  //     // event.srcElement.parentNode.children[1].focus();
  //   }, 500);
  //   this.commonService.iframe_resize();
  // }

  // selectOtherBetAmount(j, betAmt) {
  //   this.betError = '';
  //   this.otherBetAmountMultiple = (j + 1) * 10;
  //   this.otherAmtVal = '' + (this.otherBetAmountMultiple * this.gameData.betRespVOs[0].unitPrice);
  //   this.commonService.iframe_resize();
  // }

  // closeOtherAmountInput() {
  //   this.otherAmtVal = '';
  //   this.betError = '';
  //   this.otherBetAmountMultiple = 0;
  //   this.showOtherAmountInput = false;
  //   this.renderer.setElementClass(document.body, 'modal-open', false);
  //   this.commonService.iframe_resize();
  // }

  // continueOtherAmountInput() {
  //   if (this.otherAmtVal == '' && this.otherBetAmountMultiple <= 5) {
  //     this.betError = this.translate.instant('dge.beterr_cont');
  //   }
  //   if (this.betError == '') {
  //     this.otherAmtVal = '';
  //     this.betError = '';
  //     this.userData.panelData[this.panelCounter - 1].betAmountMultiple = this.otherBetAmountMultiple;
  //     this.otherBetAmountMultiple = 0;
  //     this.showOtherAmountInput = false;
  //     this.renderer.setElementClass(document.body, 'modal-open', false);
  //   }
  //   this.commonService.iframe_resize();
  // }

  /*********************************************************************************/
  /***************For Input Button specific || remove IT for modal *****************/

  selectOtherAmount(event) {
    if (!this.showOtherAmountInput) {
      this.showOtherAmountInput = true;
      this.betError = '';
      this.userData.panelData[this.panelCounter - 1].betAmountMultiple = 0;
      this.otherAmtVal = '';
      setTimeout(() => {
        document.getElementById('otherAmt-' + this.panelCounter).focus();
        this.commonService.iframe_resize();
        // event.srcElement.parentNode.children[1].focus();
      }, 500);
    }
    this.commonService.iframe_resize();
  }

  /*******************************************************************************/

  activateOtherAmt() {
    if (typeof this.userData != 'undefined') {
      let index = this.betAmtArray.indexOf(this.userData.panelData[this.panelCounter - 1].betAmountMultiple * this.gameData.betRespVOs[0].unitPrice);
      if ((index > -1 && index > 5) || (index < 0)) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  onOtherAmtInputChange(event) {
    let regex = /[^0-9.]/g;
    const initalValue = this.otherAmtVal;
    this.otherAmtVal = initalValue.replace(regex, '');
    let formatVal = parseFloat(this.otherAmtVal).toFixed(1);
    if (formatVal == 'NaN' && this.otherAmtVal == '.') {
      this.otherBetAmountMultiple = 0;
      this.otherAmtVal = '0.'
    } else if (formatVal == 'NaN' && this.otherAmtVal == '') {
      this.otherBetAmountMultiple = 0;
    } else if (formatVal == '0.0' && this.otherAmtVal != '0.0') {
      this.otherBetAmountMultiple = 0;
      if ((this.otherAmtVal.match(/[.]/g) || []).length > 1 || (this.otherAmtVal.match(/0/g) || []).length > 1) {
        this.otherAmtVal = formatVal;
      }
    } else {
      let remainder = parseFloat(formatVal) % parseFloat(this.gameData.betRespVOs[0].unitPrice);
      if (remainder == 0 && formatVal != '0.0') {
        let quotient = parseFloat(formatVal) / parseFloat(this.gameData.betRespVOs[0].unitPrice);
        if (quotient <= this.gameData.betRespVOs[0].maxBetAmtMul) {
          // if (parseFloat(formatVal) <= this.gameData.betRespVOs[0].maxBetAmtMul) {
          this.otherBetAmountMultiple = quotient;
          if ((this.otherAmtVal.match(/[.]/g) || []).length > 1) {
            this.otherAmtVal = formatVal;
          } else if (this.otherAmtVal.indexOf('.') > -1 && this.otherAmtVal.indexOf('.') != (this.otherAmtVal.length - 1)) {
            this.otherAmtVal = formatVal;
          }
          this.betError = '';
        } else {
          this.betError = this.translate.instant('dge.beterr_amt_mul_max', { mul: this.gameData.betRespVOs[0].unitPrice, max: (this.gameData.betRespVOs[0].unitPrice * this.gameData.betRespVOs[0].maxBetAmtMul) });
          // this.betError = this.translate.instant('dge.beterr_amt_mul_max', { mul: this.gameData.betRespVOs[0].unitPrice, max: this.gameData.betRespVOs[0].maxBetAmtMul });
          this.otherBetAmountMultiple = 0;
        }
      } else {
        this.betError = this.translate.instant('dge.beterr_amt_mul', { mul: this.gameData.betRespVOs[0].unitPrice });
        this.otherBetAmountMultiple = 0;
      }
    }
    /*********************For Input Button specific || remove IT for modal **********************/
    this.userData.panelData[this.panelCounter - 1].betAmountMultiple = this.otherBetAmountMultiple;
    /********************************************************************************************/
    this.commonService.iframe_resize();
  }

  addMainPanel() {
    if (this.userInfo.sessid == '-' && this.userInfo.playerid == '-' && Constants.CLIENT_MULTI_CURRENCY[Constants.CLIENT_CODE]) {
      this.commonService.openPlayerLogin();
      return false;
    }
    this.drawService.setPayoutPanelStatus(false);
    let pickedStatus = true;
    let counterUndefinedNo = 0;
    let counterUndefinedNo2 = 0;
    let counterUndefinedNoUP = 0;
    let counterUndefinedNoLP = 0;
    if (typeof this.userData.panelData[0] != 'undefined') {
      if (!this.userData.panelData[0].pickType.startsWith('Banker')) {
        for (let j = 0; j < this.userData.panelData[0].pickedValues[0].length; j++) {
          if (this.userData.panelData[0].pickedValues[0][j] == undefined) {
            ++counterUndefinedNo;
          }
        }
        for (let j = 0; j < this.userData.panelData[0].pickedValues[1].length; j++) {
          if (this.userData.panelData[0].pickedValues[1][j] == undefined) {
            ++counterUndefinedNo2;
          }
        }
      }
      if (counterUndefinedNo > 0 || counterUndefinedNo2 > 0 || counterUndefinedNoUP > 0 || counterUndefinedNoLP > 0) {
        if (this.gameCode == Constants.POWERBALL) {
          pickedStatus = false;
        }
      }
      if (!pickedStatus) {
        this.alertService.error(this.translate.instant('dge.qp_man_err'));
      }
      if (this.userData.panelData[0].betAmountMultiple == 0) {
        pickedStatus = false;
        this.alertService.error(this.translate.instant('dge.bet_amt_err'));
      }
      if (pickedStatus) {
        this.userData.panelData[0].pickedValues[0] = this.userData.panelData[0].pickedValues[0].filter(function (el) {
          return el != null;
        });
        this.userData.panelData[0].pickedValues[1] = this.userData.panelData[0].pickedValues[1].filter(function (el) {
          return el != null;
        });
        this.userData.panelData[0].pickType = this.userData.panelData[0].pickType.replace(/ QP/, '');
        this.drawService.setPanelData(this.userData.panelData[0]);
        this.removeBetPanels();
        this.resetUserData();
      }
    }
    this.commonService.iframe_resize();
  }

  numberOfLines(purchasePanelIndex) {
    if (typeof this.userData != 'undefined') {
      if (this.userData.panelData[purchasePanelIndex].betType != '') {
        let numberPickedLength = this.userData.panelData[purchasePanelIndex].pickedValues[0].filter(function (el) {
          return el != null;
        }).length;
        let numberPickedLength2 = this.userData.panelData[purchasePanelIndex].pickedValues[1].filter(function (el) {
          return el != null;
        }).length;
        if (+this.betData[this.userData.panelData[purchasePanelIndex].betType].pickTypeData.pickType[this.userData.panelData[purchasePanelIndex].pickType].range[0].pickCount.split(',')[0] == numberPickedLength &&
          +this.betData[this.userData.panelData[purchasePanelIndex].betType].pickTypeData.pickType[this.userData.panelData[purchasePanelIndex].pickType].range[1].pickCount.split(',')[0] == numberPickedLength2) {
          this.changePostState(false);
          // this.postState = false;
          return 1;
        } else {
          this.changePostState(true);
          // this.postState = true;
          return 0;
        }
      } else {
        this.changePostState(true);
        // this.postState = true;
        return 0;
      }
    }
    this.commonService.iframe_resize();
  }

  // numberOfLines(n?, r = Constants.GAME_DATA[Constants.DRAW_ENGINE][this.gameCode]['combLimit']){
  //   return this.fact(n) / (this.fact(r) * this.fact(n - r));
  // }

  fact(n) {
    let res = 1;
    for (let i = 2; i <= n; i++)
      res = res * i;
    return res;
    this.commonService.iframe_resize();
  }

  calculatePanelPrice(pIndex) {
    if (typeof this.userData != 'undefined') {
      let unitPrice = 0;
      let betAmountMultiple = 0;
      if (this.userData.panelData[pIndex].betType != '') {
        unitPrice = this.betData[this.userData.panelData[pIndex].betType].unitPrice;
        betAmountMultiple = this.userData.panelData[pIndex].betAmountMultiple;
      }
      return (unitPrice * betAmountMultiple * this.numberOfLines(pIndex));
    } else {
      return 0;
    }
    this.commonService.iframe_resize();
  }

  updateClock() {
    if (this.diff_time.totalTime > -1) {
      let diff_day = Math.floor(this.diff_time.totalTime / (3600 * 24));
      let diff_hr = Math.floor((this.diff_time.totalTime % (3600 * 24)) / 3600);
      let diff_min = Math.floor((this.diff_time.totalTime % (3600)) / 60);
      let diff_sec = Math.floor(this.diff_time.totalTime % 60);
      this.diff_time.shownTime[0] = (diff_day > 9) ? '' + diff_day : '0' + diff_day;
      this.diff_time.shownTime[1] = (diff_hr > 9) ? '' + diff_hr : '0' + diff_hr;
      this.diff_time.shownTime[2] = (diff_min > 9) ? '' + diff_min : '0' + diff_min;
      this.diff_time.shownTime[3] = (diff_sec > 9) ? '' + diff_sec : '0' + diff_sec;
      if (diff_day > 0) {
        // this.diff_time.convention = (diff_day == 1) ? 'day' : 'days';
        this.diff_time.convention = 'day';
      } else if (diff_hr > 0) {
        // this.diff_time.convention = (diff_hr == 1) ? 'hr' : 'hrs';
        this.diff_time.convention = 'hr';
      } else if (diff_min > 0) {
        // this.diff_time.convention = (diff_min == 1) ? 'min' : 'mins';
        this.diff_time.convention = 'min';
      } else if (diff_sec > 0) {
        // this.diff_time.convention = (diff_sec == 1) ? 'sec' : 'secs';
        this.diff_time.convention = 'sec';
      } else {
        this.diff_time.convention = 'sec';
        ++this.tempCounter;
        if (this.fetchDataStatus || this.tempCounter == 1) {
          localStorage.removeItem(this.gameCode + '-drawGameData');
          this.drawService.getGameData(Constants.DRAW_ENGINE, this.gameCode);
          // this.notifyDrawMachineSubscription = this.commonService.notifyDrawMachine({ 'engine': Constants.DRAW_ENGINE, 'gameCode': this.gameCode }).subscribe((response: any) => {
          // });
          // this.drawService.setLoaderStatus(true);
        }
        this.fetchDataStatus = false;
      }
      --this.diff_time.totalTime;
    }
    if (this.diff_time.saleStartTime > 0) {
      this.saleTimerStatus = true;
      this.renderer.addClass(document.body, 'modal-fix');
      let diff_day = Math.floor(this.diff_time.saleStartTime / (3600 * 24));
      let diff_hr = Math.floor((this.diff_time.saleStartTime % (3600 * 24)) / 3600);
      let diff_min = Math.floor((this.diff_time.saleStartTime % (3600)) / 60);
      let diff_sec = Math.floor(this.diff_time.saleStartTime % 60);
      this.diff_time.saleShownTime[0] = (diff_day > 9) ? '' + diff_day : '0' + diff_day;
      this.diff_time.saleShownTime[1] = (diff_hr > 9) ? '' + diff_hr : '0' + diff_hr;
      this.diff_time.saleShownTime[2] = (diff_min > 9) ? '' + diff_min : '0' + diff_min;
      this.diff_time.saleShownTime[3] = (diff_sec > 9) ? '' + diff_sec : '0' + diff_sec;
      if (diff_day > 0) {
        // this.diff_time.saleConvention = (diff_day == 1) ? 'day' : 'days';
        this.diff_time.convention = 'day';
      } else if (diff_hr > 0) {
        // this.diff_time.saleConvention = (diff_hr == 1) ? 'hr' : 'hrs';
        this.diff_time.convention = 'hr';
      } else if (diff_min > 0) {
        // this.diff_time.saleConvention = (diff_min == 1) ? 'min' : 'mins';
        this.diff_time.convention = 'min';
      } else if (diff_sec > 0) {
        // this.diff_time.saleConvention = (diff_sec == 1) ? 'sec' : 'secs';
        this.diff_time.convention = 'sec';
      } else {
        this.diff_time.saleConvention = 'sec';
      }
      --this.diff_time.saleStartTime;
    } else {
      this.saleTimerStatus = false;
      this.renderer.removeClass(document.body, 'modal-fix');
    }
  }

  changePostState(val) {
    clearInterval(this.postStateInterval);
    this.postStateInterval = setInterval(() => {
      this.postState = val;
    }, 0);
  }

  // fetchPlayerTicketList() {
  //   if (typeof this.userInfo == 'undefined' || this.userInfo.sessid == '-') {
  //     this.myTicketCount = 0;
  //     return false;
  //   }
  //   let temp = {
  //     gameCode: this.gameCode,
  //     orderBy: 'asc',
  //     pageSize: 100,
  //     pageIndex: 0,
  //     drawId: this.gameData.drawRespVOs[0].drawId,
  //   };
  //   this.ticketListSubscription = this.drawService.fetchPlayerTicketList(temp).subscribe((response: any) => {
  //     if (typeof response != 'undefined' && typeof response.data != 'undefined' && response.data.responseCode == 0 && response.data.responseData.length > 0 && response.data.responseData[0].ticketList != null) {
  //       this.myTicketCount = response.data.responseData[0].ticketList.length;
  //     } else {
  //       this.myTicketCount = 0;
  //       // this.alertService.error(response.responseMessage);
  //     }
  //   });
  // }

  navigateTicketList() {
    if (this.myTicketCount > 0) {
      this.router.navigate(['/dge/my-tickets']);
    } else {
      return false;
    }
  }

  openDrawMachine() {
    if (this.myTicketCount > 0) {
      this.router.navigate(['/dge/draw-machine'], { fragment: this.gameCode });
    } else {
      return false;
    }
  }

  checkActiveBankerPanel(numArray) {
    for (let i = 0; i < numArray.length; i++) {
      if (typeof numArray[i] == 'undefined') {
        return true;
      }
    }
    return false;
  }

  openHelpModal(gameCode) {
    this.drawService.openHelpPopup(true, gameCode);
    this.commonService.iframe_resize();
  }

  purchaseTicket() {
    this.drawService.notifyBuyPurchasePanel(true);
    this.commonService.iframe_resize();
  }

  loadBack() {
    if (this.loadLobby) {
      this.router.navigate(['/dge/lobby']);
    } else {
      this.commonService.appLobbyBack();
      this.commonService.redirectParent('loadBackUrl');
    }
  }

}