﻿import { Component, OnDestroy, OnInit, ViewEncapsulation, Renderer2 } from '@angular/core';
import { DrawService, CommonService, AuthenticationService } from '../_services';
import { ActivatedRoute, Router, ActivationEnd } from '@angular/router';
import { Constants } from '../_helpers';
import { filter, delay } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: '.draw-games',
    // styleUrls: ['./css/lms-style.css','./css/draw-ui-css.css','./css/multilineStyle.css','./css/theme-zim.css','./css/ticket.css'],
    templateUrl: 'draw.component.html',
    encapsulation: ViewEncapsulation.None
})

export class DrawComponent implements OnInit, OnDestroy {

    gameCode;
    gameData: any;
    drawData = {};
    userInfo: any;
    userData: any;
    advanceDrawPopup = false;
    advanceDrawErrorMsg = '';
    drawChecked = [];
    helpModalStatus = [];
    helpModalGameCode = '';
    routeSubscription: any;
    gameDataSubscription: any;
    advDrawPopupSubscription: any;
    openHelpPopupSubscription: any;
    userInfoLocalSubscription: any;
    subscriptionCloseModal: any;

    constructor(
        private drawService: DrawService,
        // private route: ActivatedRoute,
        private renderer: Renderer2,
        private commonService: CommonService,
        private router: Router,
        private translate: TranslateService,
        private authenticationService: AuthenticationService,
    ) {
        this.routeSubscription = router.events.pipe(
            filter(e => e instanceof ActivationEnd && typeof e.snapshot.data.gameCode != 'undefined'),
        ).subscribe(val => {
            let tempVar = <any>val;
            this.gameCode = tempVar.snapshot.data.gameCode;
            this.gameData = {};
            this.userData = {};
            this.advanceDrawErrorMsg = "";
            this.advanceDrawPopup = false;
            this.drawChecked = [];
            this.commonService.iframe_resize();
        });
        Constants.CLIENT_GAMES_MAPPING[Constants.CLIENT_CODE][Constants.DRAW_ENGINE].forEach(element => {
            this.helpModalStatus[element] = false;
        });
        // this.commonService.iframe_resize();
    }

    ngOnInit() {
        this.gameDataSubscription = this.drawService.subscribeGameData().pipe(
            delay(0),
            filter(response => response != null && typeof response.data != 'undefined' && typeof response.data.responseData.gameRespVOs[0].numberConfig != 'undefined')).subscribe((response: any) => {
                response.data.responseData.gameRespVOs.forEach(element => {
                    if (element.gameCode == this.gameCode) {
                        this.gameData = element;
                        for (let i = 0; i < element.drawRespVOs.length; i++) {
                            if (element.drawRespVOs[i].drawStatus == 'ACTIVE') {
                                this.drawData[' ' + element.drawRespVOs[i].drawId] = element.drawRespVOs[i];
                            }
                            this.drawData[' ' + element.drawRespVOs[i].drawId].drawDateTime = this.drawService.changeDateFormat(element.drawRespVOs[i].drawDateTime);
                            this.drawData[' ' + element.drawRespVOs[i].drawId].drawFreezeTime = this.drawService.changeDateFormat(element.drawRespVOs[i].drawFreezeTime);
                            this.drawData[' ' + element.drawRespVOs[i].drawId].drawSaleStopTime = this.drawService.changeDateFormat(element.drawRespVOs[i].drawSaleStopTime);
                        }
                        if (response.timerType == this.gameCode || response.timerType == '') {
                            for (let i = 0; i < this.gameData.drawRespVOs.length; i++) {
                                this.drawChecked[i] = (i != 0) ? false : true;
                            }
                            this.advanceDrawErrorMsg = "";
                            this.advanceDrawPopup = false;
                        }
                    }
                });
                this.commonService.iframe_resize();
            });

        this.advDrawPopupSubscription = this.drawService.subscribeAdvDrawPopupStatus().subscribe((response: any) => {
            this.userData = response.userData;
            if (response.status == true) {
                this.renderer.addClass(document.body, 'modal-open');
                this.drawChecked = [];
                this.advanceDrawPopup = true;
                for (let i = 0; i < this.gameData.drawRespVOs.length; i++) {
                    this.drawChecked[i] = false;
                    this.userData.drawData.forEach(element => {
                        if (element.drawId == this.gameData.drawRespVOs[i].drawId) {
                            this.drawChecked[i] = true;
                        }
                    });
                }
            } else {
                this.advanceDrawPopup = false;
                this.advanceDrawErrorMsg = "";
            }
            this.commonService.setElementTop('drawModal');
            this.commonService.iframe_resize();
        });

        this.openHelpPopupSubscription = this.drawService.subscribeOpenHelpPopup().subscribe((response: any) => {
            if (response.status == true) {
                this.openHelpModal(response.gameCode);
            }
            this.commonService.iframe_resize();
        });

        this.userInfoLocalSubscription = this.authenticationService.subscribeuserInfoLocal().subscribe((response) => {
            this.userInfo = response;
            this.commonService.iframe_resize();
        });

        this.subscriptionCloseModal = this.commonService.subscribeModalClose().subscribe(message => {
            if(!message.modalStatus){
                this.closeAdvDrawPopup();
                this.closeHelpModal();
            }
        });
    }

    ngOnDestroy(): void {
        if (typeof this.routeSubscription != 'undefined') {
            this.routeSubscription.unsubscribe();
        }
        if (typeof this.gameDataSubscription != 'undefined') {
            this.gameDataSubscription.unsubscribe();
        }
        if (typeof this.advDrawPopupSubscription != 'undefined') {
            this.advDrawPopupSubscription.unsubscribe();
        }
        if (typeof this.openHelpPopupSubscription != 'undefined') {
            this.openHelpPopupSubscription.unsubscribe();
        }
        if (typeof this.userInfoLocalSubscription != 'undefined') {
            this.userInfoLocalSubscription.unsubscribe();
        }
        if (typeof this.subscriptionCloseModal != 'undefined') {
            this.subscriptionCloseModal.unsubscribe();
        }
        this.commonService.iframe_resize();
    }

    advDrawChange(index, event) {
        let counter = 0;
        event.stopPropagation();
        for (let i = 0; i < this.gameData.drawRespVOs.length; i++) {
            if (this.drawChecked[i] == true) {
                ++counter;
            }
        }
        if (counter == 1 && this.drawChecked[index] == true) {
            this.advanceDrawErrorMsg = this.translate.instant('dge.min_draw_err');
        } if (counter == this.gameData.maxAdvanceDraws && this.drawChecked[index] == false) {
            this.advanceDrawErrorMsg = this.translate.instant('dge.max_draw_sel_err', { maxAdvanceDraws: this.gameData.maxAdvanceDraws });
        } else {
            this.drawChecked[index] = !this.drawChecked[index];
            this.advanceDrawErrorMsg = "";
        }
        this.commonService.iframe_resize();
    }

    advDrawSubmit() {
        if (this.drawChecked.indexOf(true) == -1) {
            this.advanceDrawErrorMsg = this.translate.instant('dge.min_draw_sub_err');
        } else {
            this.userData.drawData = [];
            for (let i = 0; i < this.gameData.drawRespVOs.length; i++) {
                if (this.drawChecked[i] == true) {
                    this.userData.drawData.push({ "drawId": "" + this.gameData.drawRespVOs[i].drawId });
                }
            }
            this.userData.noOfDraws = this.userData.drawData.length;
            //////////////////////////////////////////////////////////////
            this.userData.isAdvancePlay = true;
            //////////////////////////////////////////////////////////////
            this.advanceDrawPopup = false;
            this.drawService.setAdvDrawPopupSubmit(this.userData);
            this.renderer.removeClass(document.body, 'modal-open');
        }
        this.commonService.iframe_resize();
    }

    closeAdvDrawPopup() {
        this.advanceDrawPopup = false;
        this.advanceDrawErrorMsg = "";
        this.renderer.removeClass(document.body, 'modal-open');
        this.commonService.iframe_resize();
    }

    openHelpModal(gameCode) {
        this.helpModalStatus[gameCode] = true;
        this.helpModalGameCode = gameCode;
        this.commonService.setElementTop('helpModal');
        this.renderer.addClass(document.body, 'modal-open');
        this.commonService.iframe_resize();
    }

    closeHelpModal() {
        this.helpModalStatus[this.helpModalGameCode] = false;
        this.renderer.removeClass(document.body, 'modal-open');
        this.commonService.iframe_resize();
    }

}