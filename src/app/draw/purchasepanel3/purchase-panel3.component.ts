import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivationEnd } from '@angular/router';
import { DrawService, ToastService, CommonService, AlertService, AuthenticationService } from 'src/app/_services';
import { filter, delay } from 'rxjs/operators';
import { Constants } from 'src/app/_helpers';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: '.purchaseOuterWrap3',
  templateUrl: './purchase-panel3.component.html',
})
export class PurchasePanel3Component implements OnInit, OnDestroy {

  gameCode;
  gameId;
  gameData: any;
  userData: any;
  gameDataSubscription: any;
  betData = {};
  routeSubscription: any;
  panelDataSubscription: any;
  resetUserDataSubscription: any;
  updateUserDataSubscription: any;
  gameFetchCounter = 0;
  userInfo: any;
  userInfoLocalSubscription: any;
  buyNotificationSubscription: any;

  constructor(
    private drawService: DrawService,
    private alertService: AlertService,
    private commonService: CommonService,
    private router: Router,
    private toastService: ToastService,
    private translate: TranslateService,
    private authenticationService: AuthenticationService,
  ) {
    this.routeSubscription = router.events.pipe(
      filter(e => e instanceof ActivationEnd && typeof e.snapshot.data.gameCode != 'undefined'),
    ).subscribe(val => {
      this.gameCode = (<any>val).snapshot.data.gameCode;
      this.resetUserData();
      this.commonService.iframe_resize();
    });
    // this.routeSubscription = route.children[0].data.subscribe((response: any) => {
    //   this.gameCode = response.gameCode;
    // });
  }

  ngOnInit() {
    this.gameDataSubscription = this.drawService.subscribeGameData().pipe(
      delay(0),
      filter(response => response != null && typeof response.data.responseData.gameRespVOs[0].numberConfig != 'undefined')).subscribe((response: any) => {
        response.data.responseData.gameRespVOs.forEach(element => {
          if (element.gameCode == this.gameCode) {
            this.gameData = element;
            this.gameId = element.id;
            this.betData = {};
            for (let i = 0; i < element.betRespVOs.length; i++) {
              this.betData[element.betRespVOs[i].betCode] = JSON.parse(JSON.stringify(element.betRespVOs[i]));
              let tempVar = {};
              for (let j = 0; j < this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType.length; j++) {
                tempVar[element.betRespVOs[i].pickTypeData.pickType[j].code] = this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType[j];
              }
              this.betData[element.betRespVOs[i].betCode].pickTypeData.pickType = tempVar;
            }
            if (this.gameFetchCounter == 0) {
              if (response.timerType == this.gameCode || response.timerType == '') {
                this.resetUserData();
              }
            }
            setTimeout(() => {
              let tempUserData = this.commonService.getUserPurchaseData(Constants.DRAW_ENGINE);
              if (tempUserData) {
                delete tempUserData.extra;
                this.userData = JSON.parse(JSON.stringify(tempUserData));
                this.commonService.removeUserPurchaseData();
                this.drawService.setPanelCounter(this.userData.panelData.length, this.calculateTotalPrice());
                this.purchaseTicket();
              }
            }, 0);
            ++this.gameFetchCounter;
          }
        });
        this.commonService.iframe_resize();
      });

    this.panelDataSubscription = this.drawService.subscribePanelData().subscribe((response: any) => {
      if (this.userData.panelData.length < this.gameData.maxPanelAllowed) {
        this.userData.panelData.push(response.panelData);
        this.userData.noOfPanel = this.userData.panelData.length;
        this.toastService.success(this.translate.instant('dge.bet_add_succ'));
      } else {
        this.toastService.error(this.translate.instant('dge.max_pan_err', { maxPanelAllowed: this.gameData.maxPanelAllowed }));
      }
      this.drawService.setPanelCounter(this.userData.panelData.length, this.calculateTotalPrice());
      this.commonService.iframe_resize();
    });

    this.resetUserDataSubscription = this.drawService.subscribeResetUserData().subscribe((response: any) => {
      if (response.status) {
        this.resetUserData();
      }
      this.drawService.setPanelCounter(this.userData.panelData.length, this.calculateTotalPrice());
      this.commonService.iframe_resize();
    });

    this.updateUserDataSubscription = this.drawService.subscribeUpdateUserData().subscribe((response: any) => {
      this.userData = response.userData;
      this.drawService.setPanelCounter(this.userData.panelData.length, this.calculateTotalPrice());
      this.commonService.iframe_resize();
    });

    this.userInfoLocalSubscription = this.authenticationService.subscribeuserInfoLocal().subscribe((response) => {
      this.userInfo = response;
      this.commonService.iframe_resize();
    });

    this.buyNotificationSubscription = this.drawService.subscribeBuyNotification().subscribe((response) => {
      this.purchaseTicket();
      this.commonService.iframe_resize();
    });

  }

  ngOnDestroy(): void {
    if (typeof this.gameDataSubscription != 'undefined') {
      this.gameDataSubscription.unsubscribe();
    }
    if (typeof this.routeSubscription != 'undefined') {
      this.routeSubscription.unsubscribe();
    }
    if (typeof this.panelDataSubscription != 'undefined') {
      this.panelDataSubscription.unsubscribe();
    }
    if (typeof this.resetUserDataSubscription != 'undefined') {
      this.resetUserDataSubscription.unsubscribe();
    }
    if (typeof this.updateUserDataSubscription != 'undefined') {
      this.updateUserDataSubscription.unsubscribe();
    }
    if (typeof this.userInfoLocalSubscription != 'undefined') {
      this.userInfoLocalSubscription.unsubscribe();
    }
    if (typeof this.buyNotificationSubscription != 'undefined') {
      this.buyNotificationSubscription.unsubscribe();
    }
    this.commonService.iframe_resize();
  }

  resetUserData() {
    this.userData = {
      "currencyCode": (typeof this.gameData != 'undefined' && typeof this.gameData.currencyRespVOs != 'undefined') ? this.gameData.currencyRespVOs[0].currencyCode : '',
      // "drawData": [{"drawId" : ""+this.gameData.drawRespVOs[0].drawId}],
      "drawData": [],
      "gameCode": this.gameCode,
      "gameId": (typeof this.gameData != 'undefined' && typeof this.gameData.id != 'undefined') ? this.gameId : 0,
      "isAdvancePlay": false,
      "lastTicketNumber": 0,
      // "noOfDraws": 1,
      "noOfDraws": 1,
      "noOfPanel": 0,
      "panelData": [],
    }
    this.drawService.setPanelCounter(this.userData.panelData.length, this.calculateTotalPrice());
    this.commonService.iframe_resize();
  }

  numberOfLines(purchasePanelIndex) {
    return 1;
    this.commonService.iframe_resize();
  }

  // numberOfLines(n?, r = Constants.GAME_DATA[Constants.DRAW_ENGINE][this.gameCode]['combLimit']){
  //   return this.fact(n) / (this.fact(r) * this.fact(n - r)); 
  // }

  fact(n) {
    let res = 1;
    for (let i = 2; i <= n; i++)
      res = res * i;
    return res;
    this.commonService.iframe_resize();
  }

  calculatePanelPrice(pIndex) {
    if (typeof this.userData != 'undefined') {
      let unitPrice = this.betData[this.userData.panelData[pIndex].betType].unitPrice;
      let betAmountMultiple = this.userData.panelData[pIndex].betAmountMultiple;
      return (unitPrice * betAmountMultiple * this.numberOfLines(pIndex) * this.userData.noOfDraws);
    }
    return 0;
    this.commonService.iframe_resize();
  }

  calculateTotalPrice() {
    if (typeof this.userData != 'undefined') {
      let totalSum = 0;
      for (let i = 0; i < this.userData.panelData.length; i++) {
        totalSum += this.calculatePanelPrice(i);
      }
      return totalSum;
    }
    return 0;
    this.commonService.iframe_resize();
  }

  purchaseTicket() {
    if (this.userInfo.sessid == '-' && this.userInfo.playerid == '-' && Constants.CLIENT_MULTI_CURRENCY[Constants.CLIENT_CODE]) {
      this.commonService.openPlayerLogin();
      return false;
    }
    if (this.userData.noOfPanel == 0) {
      this.alertService.error(this.translate.instant('dge.add_bet_err'));
      return false;
    }
    this.drawService.setTicketStatus(true, this.userData);
    this.commonService.iframe_resize();
  }

}