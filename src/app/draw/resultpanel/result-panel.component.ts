import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivationEnd } from '@angular/router';
import { Constants } from 'src/app/_helpers';
import { filter } from 'rxjs/operators';
import { DrawService, CommonService } from 'src/app/_services';

@Component({
  selector: '.lastResultWrap',
  templateUrl: './result-panel.component.html'
})
export class ResultPanelComponent implements OnInit, OnDestroy {

  gameCode;
  gameData: any;
  gameDataSubscription: any;
  routeSubscription: any;
  lastDrawResult: any;

  constructor(
    private drawService: DrawService,
    private router: Router,
    private commonService: CommonService
  ) {    
    this.routeSubscription = router.events.pipe(
      filter(e => e instanceof ActivationEnd && typeof e.snapshot.data.gameCode != 'undefined'),
    ).subscribe(val => {
      this.gameCode = (<any>val).snapshot.data.gameCode;
      this.commonService.iframe_resize();
    });
  }

  ngOnInit() {
    this.gameDataSubscription = this.drawService.subscribeGameData().pipe(
      filter(response => response != null && typeof response.data.responseData.gameRespVOs[0].numberConfig != 'undefined')).subscribe((response: any) => {
      response.data.responseData.gameRespVOs.forEach(element => {
        if (element.gameCode == this.gameCode) {
          this.lastDrawResult = [];
          this.gameData = element;
          if(this.gameData.lastDrawWinningResultVOs.length == 0 && typeof Constants.CLIENT_RESULTS_HARDCODE[Constants.CLIENT_CODE][Constants.DRAW_ENGINE][this.gameCode] != 'undefined'){
            this.gameData.lastDrawWinningResultVOs = Constants.CLIENT_RESULTS_HARDCODE[Constants.CLIENT_CODE][Constants.DRAW_ENGINE][this.gameCode];
          }
          this.gameData.lastDrawWinningResultVOs.forEach(element => {
            element.lastDrawDateTime = this.drawService.changeDateFormat(element.lastDrawDateTime);
          });
          if(element.gameCode == Constants.THAI_LOTTERY || element.gameCode == Constants.LOTTERY_DIAMOND){
            let temp = this.gameData.lastDrawWinningResultVOs[0].winningNumber.split(',');
            this.lastDrawResult = [
              temp[0].split('#').join(''),
              temp[1].split('#').join(''),
              temp[2].split('#').join(''),
              temp[3].split('#').join(''),
              temp[4].split('#').join(''),
              temp[5].split('#').join(''),
              temp[6].split('#').filter(function(el){
                return el != -1;
              }).join(''),
            ];
          }
        }
      });
    });
  }

  ngOnDestroy(): void {
    if (typeof this.routeSubscription != 'undefined') {
      this.routeSubscription.unsubscribe();
    }
    if (typeof this.gameDataSubscription != 'undefined') {
      this.gameDataSubscription.unsubscribe();
    }
  }

  getLastDrawResult(i){
    if(typeof this.lastDrawResult != 'undefined'){
      return this.lastDrawResult[i];
    }else{
      return '';
    }
  }

}