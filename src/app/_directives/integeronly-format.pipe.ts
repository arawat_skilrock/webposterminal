import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'integerOnly'
})
export class IntegeronlyPipe implements PipeTransform {
  transform(value: string): string {
    let newStr: string = value.match(/\d+/g)[0];
    if(newStr == '0') {
      newStr = 'Zero';
    }   
    return newStr;
  }
}