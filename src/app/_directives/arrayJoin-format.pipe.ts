import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayJoin',
  pure: false
})
export class ArrayjoinPipe implements PipeTransform {
  transform(value: []): string {
    if(value == null){
      return '';
    }
    let newStr = value.filter(function (el) {
      return el != null;
    }).join();
    return newStr;
  }
}