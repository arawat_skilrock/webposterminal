import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from "@angular/common";
import { Constants } from '../_helpers';

@Pipe({ name: 'custCurrency' })
export class CustomCurrencyPipe extends CurrencyPipe implements PipeTransform {
    transform(value: string, currencyCode = '', returnType = 'html'): any {
        if (currencyCode == '') {
            let tempUser = JSON.parse(localStorage.getItem('drawGameUserInfo'));
            if (tempUser != null) {
                currencyCode = tempUser.currDisplay;
            } else {
                currencyCode = 'USD';
            }
        }
        let locale = 'en-US';
        locale = Constants.CLIENT_CURRENCY_LOCALE_MAPPING[Constants.CLIENT_CODE];
        let currencyValue = super.transform(value, currencyCode, 'symbol-narrow', "1.2-2", locale);
        if (returnType == 'html' && currencyValue) {
            if (!/\d/.test(currencyValue.substr(0, 4))) {
                return '<span class="currency">' + currencyValue.substr(0, 4) + '</span><span class="value">' + currencyValue.substr(4) + '</span>';
            }
            if (!/\d/.test(currencyValue.substr(0, 3))) {
                return '<span class="currency">' + currencyValue.substr(0, 3) + '</span><span class="value">' + currencyValue.substr(3) + '</span>';
            }
            if (!/\d/.test(currencyValue.substr(0, 2))) {
                return '<span class="currency">' + currencyValue.substr(0, 2) + '</span><span class="value">' + currencyValue.substr(2) + '</span>';
            }
            if (!/\d/.test(currencyValue.substr(0, 1))) {
                let tempCurrCode = currencyValue.substr(0, 1);
                if (tempCurrCode == '-') {
                    tempCurrCode = '';
                }
                return '<span class="currency">' + tempCurrCode + '</span><span class="value">' + currencyValue.substr(1) + '</span>';
            }
            if (!/\d/.test(currencyValue.substr(-4))) {
                return '<span class="value">' + currencyValue.substr(0, currencyValue.length - 4) + '</span><span class="currency">' + currencyValue.substr(-4) + '</span>';
            }
            if (!/\d/.test(currencyValue.substr(-3))) {
                return '<span class="value">' + currencyValue.substr(0, currencyValue.length - 3) + '</span><span class="currency">' + currencyValue.substr(-3) + '</span>';
            }
            if (!/\d/.test(currencyValue.substr(-2))) {
                return '<span class="value">' + currencyValue.substr(0, currencyValue.length - 2) + '</span><span class="currency">' + currencyValue.substr(-2) + '</span>';
            }
            if (!/\d/.test(currencyValue.substr(-1))) {
                let tempCurrCode = currencyValue.substr(-1);
                if (tempCurrCode == '-') {
                    tempCurrCode = '';
                }
                return '<span class="value">' + currencyValue.substr(0, currencyValue.length - 1) + '</span><span class="currency">' + currencyValue.substr(-1) + '</span>';
            }
        }
        return currencyValue;
    }
}