import { Component, OnInit, OnDestroy, Inject, Renderer2 } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService, DrawService, CommonService } from '../../_services';
import { Router } from '@angular/router';
// import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'alert',
    templateUrl: 'alert.component.html',
    styleUrls: ['../css/common.css']
})

export class AlertComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    message: any;
    messageType: any;
    ticketNumber: any;
    xlinkHref: any;
    clearTimeoutVar: any;
    url: any;
    subscriptionCloseModal: any;
    redirectParent: any;

    constructor(
        private alertService: AlertService,
        private renderer: Renderer2,
        private router: Router,
        private drawService: DrawService,
        private commonService: CommonService,
        // @Inject(DOCUMENT) private document: Document
    ) { }

    ngOnInit() {
        this.subscription = this.alertService.getMessage().subscribe(message => {
            if (message.type != '') {
                if (message.type == 'clear') {
                    message = null;
                }
                this.message = message;
                this.messageType = message ? message.type.toLowerCase() : "error";
                this.url = message ? (message.url ? message.url : "") : "";
                this.redirectParent = message ? (message.redirectParent ? message.redirectParent : "") : "";
                this.ticketNumber = message ? (message.ticketNumber ? message.ticketNumber : "") : "";
                this.xlinkHref = "assets/images/sprite-sheet.svg#icon-" + this.messageType;
                this.renderer.addClass(document.body, 'modal-open');
                if (this.url == '' && this.ticketNumber == '' && this.redirectParent == '') {
                    clearTimeout(this.clearTimeoutVar);
                    this.clearTimeoutVar = setTimeout(() => {
                        this.message = null;
                        this.renderer.removeClass(document.body, 'modal-open');
                    }, 5000);
                }
            } else {
                message = null;
                this.message = message;
                this.messageType = message ? message.type.toLowerCase() : "error";
                this.url = message ? (message.url ? message.url : "") : "";
                this.redirectParent = message ? (message.redirectParent ? message.redirectParent : "") : "";
                this.ticketNumber = message ? (message.ticketNumber ? message.ticketNumber : "") : "";
                this.xlinkHref = "assets/images/sprite-sheet.svg#icon-" + this.messageType;
            }
            this.commonService.setElementTop('popupMsg');
            this.commonService.iframe_resize();
        });

        this.subscriptionCloseModal = this.commonService.subscribeModalClose().subscribe(message => {
            if (!message.modalStatus) {
                this.closeModal();
            }
            this.commonService.iframe_resize();
        });
    }

    closeModal() {
        this.message = null;
        this.renderer.removeClass(document.body, 'modal-open');
        clearTimeout(this.clearTimeoutVar);
        this.commonService.iframe_resize();
    }

    cancelModal() {

    }

    okModal() {

    }

    redirectToUrl() {
        this.message = null;
        this.renderer.removeClass(document.body, 'modal-open');
        clearTimeout(this.clearTimeoutVar);
        this.drawService.fromMyTickets = false;
        this.router.navigate([this.url]);
        this.commonService.iframe_resize();
    }

    redirectToParent(parentUrl) {
        this.message = null;
        this.renderer.removeClass(document.body, 'modal-open');
        clearTimeout(this.clearTimeoutVar);
        this.commonService.redirectParent(parentUrl);
        this.commonService.iframe_resize();
    }

    ngOnDestroy() {
        clearTimeout(this.clearTimeoutVar);
        if (typeof this.subscription != 'undefined') {
            this.subscription.unsubscribe();
        }
        if (typeof this.subscriptionCloseModal != 'undefined') {
            this.subscriptionCloseModal.unsubscribe();
        }
    }

}