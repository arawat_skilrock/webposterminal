import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DrawService, AlertService, ChatService } from '../_services';
import { AlertComponent } from './alert';
import { ToastComponent } from './toast';
import { IntegeronlyPipe, ArrayjoinPipe, OrdinalPipe, CurrencySymbolPipe, VarDirective, CustomCurrencyPipe, MyFilterPipe } from '../_directives';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { PickerModule, EmojiFrequentlyService, EmojiSearch } from '@ctrl/ngx-emoji-mart';
import { EmojiModule, EmojiService } from '@ctrl/ngx-emoji-mart/ngx-emoji';

@NgModule({
  declarations: [
    AlertComponent,
    ToastComponent,
    IntegeronlyPipe,
    ArrayjoinPipe,
    OrdinalPipe,
    CurrencySymbolPipe,
    CustomCurrencyPipe,
    VarDirective,
    MyFilterPipe,
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    FormsModule,
    PickerModule,
    EmojiModule
  ],
  exports: [
    AlertComponent,
    ToastComponent,
    IntegeronlyPipe,
    ArrayjoinPipe,
    OrdinalPipe,
    CurrencySymbolPipe,
    CustomCurrencyPipe,
    VarDirective,
    MyFilterPipe
  ],
  providers: [DrawService, AlertService, ChatService, EmojiFrequentlyService, EmojiSearch, EmojiService]
})

export class SimilarModule { }