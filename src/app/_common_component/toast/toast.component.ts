import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ToastService } from '../../_services';

@Component({
    selector: 'toast',
    templateUrl: 'toast.component.html',
    styleUrls: ['../css/common.css']
})

export class ToastComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    message: any;
    messageType: any;
    clearTimeoutVar: any;

    constructor(private toastService: ToastService) { }

    ngOnInit() {
        this.subscription = this.toastService.getMessage().subscribe(message => {
            this.message = message;
            message ? this.messageType = message.type.toLowerCase() : this.messageType = "error";
            clearTimeout(this.clearTimeoutVar);
            this.clearTimeoutVar = setTimeout(() => {
                this.message = null;
            }, 5000);
        });
    }

    ngOnDestroy() {
        clearTimeout(this.clearTimeoutVar);
        if (typeof this.subscription != 'undefined') {
            this.subscription.unsubscribe();
        }
    }
}
