import { environment } from 'src/environments/environment';

export class Hardcode {

    static readonly DRAW_FETCH_GAME_DATA = {
        "success": true,
        "data": {
            "responseCode": 0,
            "responseMessage": "Success",
            "responseData": {
                "gameRespVOs": [
                    {
                        "id": 2,
                        "gameNumber": 2,
                        "gameName": "EURO MILLION",
                        "gameCode": "euroMillion",
                        "betLimitEnabled": "NO",
                        "familyCode": "MultiSet",
                        "lastDrawResult": "37,13,14,04,34#04,12",
                        "displayOrder": "2",
                        "drawFrequencyType": "",
                        "timeToFetchUpdatedGameInfo": "2021-11-10 19:54:00",
                        "numberConfig": {
                            "range": [
                                {
                                    "ball": [
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "01"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "02"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "03"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "04"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "05"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "06"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "07"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "08"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "09"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "10"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "11"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "12"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "13"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "14"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "15"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "16"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "17"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "18"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "19"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "20"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "21"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "22"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "23"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "24"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "25"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "26"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "27"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "28"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "29"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "30"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "31"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "32"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "33"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "34"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "35"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "36"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "37"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "38"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "39"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "40"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "41"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "42"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "43"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "44"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "45"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "46"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "47"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "48"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "49"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "50"
                                        }
                                    ]
                                },
                                {
                                    "ball": [
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "00"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "01"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "02"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "03"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "04"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "05"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "06"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "07"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "08"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "09"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "10"
                                        },
                                        {
                                            "color": "",
                                            "label": "",
                                            "number": "11"
                                        }
                                    ]
                                }
                            ]
                        },
                        "betRespVOs": [
                            {
                                "unitPrice": 50,
                                "maxBetAmtMul": 1,
                                "betDispName": "Direct7",
                                "betCode": "Direct7",
                                "betName": "Direct7",
                                "betGroup": null,
                                "pickTypeData": {
                                    "pickType": [
                                        {
                                            "name": "Manual",
                                            "code": "Direct7",
                                            "range": [
                                                {
                                                    "pickMode": "Simple",
                                                    "pickCount": "5,5",
                                                    "pickValue": "",
                                                    "pickConfig": "Number",
                                                    "qpAllowed": "YES"
                                                },
                                                {
                                                    "pickMode": "Simple",
                                                    "pickCount": "2,2",
                                                    "pickValue": "",
                                                    "pickConfig": "Number",
                                                    "qpAllowed": "YES"
                                                }
                                            ],
                                            "coordinate": null,
                                            "description": null
                                        }
                                    ]
                                },
                                "inputCount": "5#2",
                                "winMode": "MAIN",
                                "betOrder": 1
                            }
                        ],
                        "drawRespVOs": [
                            {
                                "drawId": 7756,
                                "drawName": "SABANZURI Lotto",
                                "drawDay": "TUESDAY",
                                "drawDateTime": "2021-11-10 19:55:00",
                                "drawSaleStartTime": "2020-11-10 19:34:00",
                                "drawFreezeTime": "2021-11-10 19:54:55",
                                "drawSaleStopTime": "2021-11-10 19:54:00",
                                "drawStatus": "ACTIVE"
                            },
                            {
                                "drawId": 7757,
                                "drawName": "SABANZURI Lotto",
                                "drawDay": "TUESDAY",
                                "drawDateTime": "2021-11-10 20:05:00",
                                "drawSaleStartTime": "2021-11-10 20:44:00",
                                "drawFreezeTime": "2021-11-10 20:04:55",
                                "drawSaleStopTime": "2021-11-10 20:04:00",
                                "drawStatus": "ACTIVE"
                            },
                            {
                                "drawId": 7758,
                                "drawName": "SABANZURI Lotto",
                                "drawDay": "TUESDAY",
                                "drawDateTime": "2021-11-10 21:15:00",
                                "drawSaleStartTime": "2021-11-10 21:54:00",
                                "drawFreezeTime": "2021-11-10 21:14:55",
                                "drawSaleStopTime": "2021-11-10 21:14:00",
                                "drawStatus": "ACTIVE"
                            },
                            {
                                "drawId": 7759,
                                "drawName": "SABANZURI Lotto",
                                "drawDay": "TUESDAY",
                                "drawDateTime": "2021-11-10 22:25:00",
                                "drawSaleStartTime": "2021-11-10 22:04:00",
                                "drawFreezeTime": "2021-11-10 22:24:55",
                                "drawSaleStopTime": "2021-11-10 22:24:00",
                                "drawStatus": "ACTIVE"
                            },
                            {
                                "drawId": 7760,
                                "drawName": "SABANZURI Lotto",
                                "drawDay": "TUESDAY",
                                "drawDateTime": "2021-11-10 23:35:00",
                                "drawSaleStartTime": "2021-11-10 23:14:00",
                                "drawFreezeTime": "2021-11-10 23:34:55",
                                "drawSaleStopTime": "2021-11-10 23:34:00",
                                "drawStatus": "ACTIVE"
                            }
                        ],
                        "nativeCurrency": "KSH",
                        "drawEvent": "SHARED_DRAW",
                        "gameStatus": "SALE_OPEN",
                        "gameOrder": "2",
                        "consecutiveDraw": "1,2,3,4,5",
                        "maxAdvanceDraws": 5,
                        "drawPrizeMultiplier": {
                            "createEvent": null,
                            "applyOnBet": "MAIN",
                            "multiplier": null
                        },
                        "lastDrawFreezeTime": "",
                        "lastDrawDateTime": "",
                        "lastDrawSaleStopTime": "2020-11-10 13:44:55",
                        "lastDrawTime": "2020-11-10 13:25:00",
                        "ticket_expiry": 15,
                        "currencyRespVOs": [
                            {
                                "currencyCode": "KSH",
                                "value": 1
                            }
                        ],
                        "lastDrawWinningResultVOs": [
                            {
                                "lastDrawDateTime": "2020-11-10 13:25:00.0",
                                "winningNumber": "37,13,14,04,34#04,11",
                                "winningMultiplierInfo": {
                                    "multiplierCode": null,
                                    "value": null
                                },
                                "runTimeFlagInfo": [

                                ],
                                "sideBetMatchInfo": [

                                ],
                                "drawId": 0,
                                "currentDrawStatus": null
                            },
                            {
                                "lastDrawDateTime": "2020-11-10 13:15:00.0",
                                "winningNumber": "18,05,31,15,23#00,11",
                                "winningMultiplierInfo": {
                                    "multiplierCode": null,
                                    "value": null
                                },
                                "runTimeFlagInfo": [

                                ],
                                "sideBetMatchInfo": [

                                ],
                                "drawId": 0,
                                "currentDrawStatus": null
                            },
                            {
                                "lastDrawDateTime": "2020-11-10 13:05:00.0",
                                "winningNumber": "05,13,38,20,11#00,11",
                                "winningMultiplierInfo": {
                                    "multiplierCode": null,
                                    "value": null
                                },
                                "runTimeFlagInfo": [

                                ],
                                "sideBetMatchInfo": [

                                ],
                                "drawId": 0,
                                "currentDrawStatus": null
                            },
                            {
                                "lastDrawDateTime": "2020-11-10 12:55:00.0",
                                "winningNumber": "24,42,11,0116#04,11",
                                "winningMultiplierInfo": {
                                    "multiplierCode": null,
                                    "value": null
                                },
                                "runTimeFlagInfo": [

                                ],
                                "sideBetMatchInfo": [

                                ],
                                "drawId": 0,
                                "currentDrawStatus": null
                            },
                            {
                                "lastDrawDateTime": "2020-11-10 12:45:00.0",
                                "winningNumber": "03,05,23,11,32#03,11",
                                "winningMultiplierInfo": {
                                    "multiplierCode": null,
                                    "value": null
                                },
                                "runTimeFlagInfo": [

                                ],
                                "sideBetMatchInfo": [

                                ],
                                "drawId": 0,
                                "currentDrawStatus": null
                            }
                        ],
                        "maxPanelAllowed": 8,
                        "gameSchemas": {
                            "gameDevName": "powerball",
                            "matchDetail": [
                                {
                                    "match": "Direct7",
                                    "rank": 1,
                                    "type": "PARIMUTUEL",
                                    "prizeAmount": "45.0",
                                    "betType": "Direct7",
                                    "pattern": null
                                },
                                {
                                    "match": "Direct7",
                                    "rank": 2,
                                    "type": "PARIMUTUEL",
                                    "prizeAmount": "20.0",
                                    "betType": "Direct7",
                                    "pattern": null
                                },
                                {
                                    "match": "Direct7",
                                    "rank": 3,
                                    "type": "PARIMUTUEL",
                                    "prizeAmount": "35.0",
                                    "betType": "Direct7",
                                    "pattern": null
                                },
                                {
                                    "match": "Direct7",
                                    "rank": 4,
                                    "type": "FIXED",
                                    "prizeAmount": "25000.0",
                                    "betType": "Direct7",
                                    "pattern": null
                                },
                                {
                                    "match": "Direct7",
                                    "rank": 5,
                                    "type": "FIXED",
                                    "prizeAmount": "5000.0",
                                    "betType": "Direct7",
                                    "pattern": null
                                },
                                {
                                    "match": "Direct7",
                                    "rank": 6,
                                    "type": "FIXED",
                                    "prizeAmount": "500.0",
                                    "betType": "Direct7",
                                    "pattern": null
                                },
                                {
                                    "match": "Direct7",
                                    "rank": 7,
                                    "type": "FIXED",
                                    "prizeAmount": "250.0",
                                    "betType": "Direct7",
                                    "pattern": null
                                },
                                {
                                    "match": "Direct7",
                                    "rank": 8,
                                    "type": "FIXED",
                                    "prizeAmount": "150.0",
                                    "betType": "Direct7",
                                    "pattern": null
                                },
                                {
                                    "match": "Direct7",
                                    "rank": 9,
                                    "type": "FIXED",
                                    "prizeAmount": "150.0",
                                    "betType": "Direct7",
                                    "pattern": null
                                }
                            ]
                        },
                        "resultConfigData": {
                            "type": "RNG",
                            "balls": "5#2",
                            "ballsPerCall": 7,
                            "interval": 0,
                            "duplicateAllowed": false
                        }
                    }
                ],
                "currentDate": "2020-11-10 19:44:01"
            }
        },
        "timerType": "euroMillion"
    };

    static readonly DRAW_GET_PRIZE_SCHEME = {};

    static readonly DRAW_GET_QUICK_PICK = {};

    static readonly DRAW_TICKET_SALE = {};

    static readonly WINNING_CLAIM = {};

    static readonly PLAYER_TICKET_LIST = {};

    static readonly TICKET_STATUS = {};

    static readonly TRACK_TICKET = {};

    static readonly RESULTS = {};

}