var iframeHeight;
var iframe_id;
var iframe_div_id;

(function () {
    if (typeof (LottoGames) == 'undefined')
        LottoGames = function () { };
    LottoGames.bind = function (method, func) {
        if (typeof (LottoGames[method]) == 'undefined')
            LottoGames[method] = func;
        return LottoGames[method]
    };
    LottoGames.bind('i', 0);
    LottoGames.setts = {};
    LottoGames.bind('frame', function (_ic) {
        for (r = 0; r < _ic.length; r++)
            LottoGames.setts[_ic[r][0]] = _ic[r][1];
        LottoGames.i++;
        try {
            bFrame = document.createElement('<iframe frameborder=0 id="lottogames_iframe_' + LottoGames.i + '" name="lottogames_iframe_' + LottoGames.i + '">');
        } catch (e) {
            bFrame = document.createElement('iframe');
            bFrame.name = 'lottogames_iframe_' + LottoGames.i;
            bFrame.setAttribute('frameborder', '0');
        }
        bFrame.id = 'lottogames_iframe_' + LottoGames.i;
        iframe_id = 'lottogames_iframe_' + LottoGames.i;
        bFrame.setAttribute('allowFullScreen', '');
        bFrame.scrolling = "no";
        //bframe.onload = "iframeLoaded();";
        bFrame.src = LottoGames.bind('make_location', function (sets) {
            sets = LottoGames.setts;
            result = sets.server + sets.gametype;
            iframe_div_id = sets.iframe_div_id;
            if (sets.lobby_code) {
                result += '/' + sets.lobby_code.toLowerCase();
            }
            if (sets.ticketno) {
                result += '/' + sets.ticketno;
            }
            if (sets.transactionId) {
                result += '/' + sets.transactionId;
            }
            if (sets.vendor) {
                result += '/' + sets.vendor.toUpperCase();
            }
            if (sets.game_name) {
                result += '/' + sets.game_name.toLowerCase().replace(/[ /]/g, '-');
            }
            if (sets.game_id) {
                result += '/' + sets.game_id;
            }
            if (sets.play_type) {
                result += '/' + sets.play_type.toLowerCase();
            }
            if (sets.player_id) {
                result += '/' + sets.player_id;
            } else {
                result += '/' + '-';
            }
            if (sets.player_name) {
                result += '/' + sets.player_name;
            } else {
                result += '/' + '-';
            }
            if (sets.session_id) {
                result += '/' + sets.session_id;
            } else {
                result += '/' + '-';
            }
            if (sets.balance) {
                result += '/' + sets.balance;
            } else {
                result += '/' + '-';
            }
            if (sets.language) {
                result += '/' + sets.language;
            } else {
                result += '/' + 'en';
            }
            if (sets.currency) {
                result += '/' + sets.currency;
            } else {
                result += '/' + 'USD';
            }
            if (sets.currencyDisplay) {
                result += '/' + sets.currencyDisplay;
            } else {
                result += '/' + '-';
            }
            if (sets.alias) {
                result += '/' + sets.alias;
            } else {
                result += '/' + '-';
            }
            if (sets.isMobileApp) {
                result += '/' + sets.isMobileApp;
            } else {
                result += '/' + 0;
            }
            if (sets.return_url) {
                result += '#' + sets.return_url;
            }
            if (sets.gameCode) {
                result += '#' + sets.gameCode;
            }
            return result;
        })(_ic)
        // if (LottoGames.width) {
        //     bFrame.width = LottoGames.width;
        // } else {
        //     bFrame.width = '100%';
        // }
        // if (LottoGames.height) {
        //     bFrame.height = LottoGames.height;
        // } else {
        //     bFrame.height = '770px';
        // }
        // if ((typeof window.screen.orientation != 'undefined' && window.screen.orientation.type == 'portrait-primary') || (typeof window.orientation != 'undefined' && (window.orientation == 0 || window.orientation == 180))) {
        //     bFrame.style.width = '100%';
        //     bFrame.style.height = (window.screen.width + 50) + 'px';
        // } else if ((typeof window.screen.orientation != 'undefined' && window.screen.orientation.type == 'landscape-primary') || (typeof window.orientation != 'undefined' && (window.orientation == 90 || window.orientation == -90))) {
        //     bFrame.style.width = '100%';
        //     bFrame.style.height = (window.screen.width - 50) + 'px';
        // }
        if (window.screen.width < window.screen.height) {
            bFrame.style.width = '100%';
            bFrame.style.height = (window.screen.width + 50) + 'px';
        } else {
            bFrame.style.width = '100%';
            bFrame.style.height = (window.screen.width - 50) + 'px';
        }
        if (document.getElementById(iframe_div_id) === null) {
            document.write('<div id="' + iframe_div_id + '"></div>');
        }
        document.getElementById(iframe_div_id).appendChild(bFrame);
        iframeHeight = document.getElementById(iframe_id).height;
    });
})();

var zino_resize = function (event) {
    var zino_iframe = document.getElementById(iframe_id);
    if (zino_iframe) {
        if (event.data.msg == 'scrollTop') {
            var myObj = new Object();
            myObj.msg = 'storePrev';
            myObj.scrollHeight = window.pageYOffset;
            zino_iframe.contentWindow.postMessage(myObj, "*");
            window.scrollTo(0, zino_iframe.scrollTop);
        } else if (event.data.msg == 'scrollPrev') {
            var myObj = new Object();
            myObj.msg = 'storePrev';
            myObj.scrollHeight = 0;
            zino_iframe.contentWindow.postMessage(myObj, "*");
            window.scrollTo(0, event.data.scrollHeight);
        } else if (event.data.msg == 'errorScrollHeight') {
            // var myObj = new Object();
            // myObj.msg = 'errorScrollheight';
            // var iframeElement = document.getElementById(iframe_id);
            // var elementHeight = iframeElement.offsetHeight;
            // var windowPageYOffset = window.pageYOffset;
            // if (windowPageYOffset > elementHeight - 300) {
            //     myObj.scrollHeight = elementHeight / 2;
            // } else {
            //     myObj.scrollHeight = window.pageYOffset;
            // }
            // zino_iframe.contentWindow.postMessage(myObj, "*");
        } else if (event.data.msg == 'browserSupport') {
            var myObj = new Object();
            myObj.msg = 'browserSupport';
            if (document.body.classList.contains('supported')) {
                myObj.status = true;
            } else {
                myObj.status = false;
            }
            var iframeElement = document.getElementById(iframe_id);
            var elementHeight = getPosition(iframeElement).top;
            myObj.topHeight = elementHeight;
            zino_iframe.contentWindow.postMessage(myObj, "*");
        } else if (event.data.msg == 'iframeParentScrollTop') {
            var myObj = new Object();
            myObj.msg = 'iframeParentScrollTop';
            var iframeParentElement = document.getElementById(iframe_div_id);
            var scrollTopHeight = iframeParentElement.scrollTop;
            var clientFrameHeight = iframeParentElement.clientHeight;
            myObj.clientFrameHeight = clientFrameHeight;
            myObj.scrollTopHeight = scrollTopHeight;
            zino_iframe.contentWindow.postMessage(myObj, "*");
        } else if (event.data.msg == 'bodyClass') {
            if (event.data.class == 'iframeGamePlay') {
                document.body.classList.add('iframeGamePlay');
                zino_iframe.setAttribute('scrolling', 'yes');
            } else {
                document.body.classList.remove('iframeGamePlay');
                zino_iframe.setAttribute('scrolling', 'no');
            }
        } else if (event.data.msg == "updatePlayerBalance") {
            // window.parent.updatePlayerBalance('both');
            if (typeof updatePlayerBalance == 'function') {
                updatePlayerBalance('both');
            }
        } else if (event.data.msg == "clientLogin") {
            if (typeof openLoginModal == 'function') {
                openLoginModal();
            }
            if (typeof Mobile != 'undefined' && Mobile.showLoginDialog == 'function') {
                Mobile.showLoginDialog()
            }
        } else if (event.data.msg == "loadBackUrl") {
            window.history.back();
        } else if (event.data.msg == "loadDGE") {
            if (typeof openDGEUrl == 'function') {
                openDGEUrl();
            }
        } else if (event.data.msg == "loadSBS") {
            if (typeof openSBSUrl == 'function') {
                openSBSUrl();
            }
        } else if (event.data.msg == "loadLIVEBET") {
            if (typeof openLIVEBETUrl == 'function') {
                openLIVEBETUrl();
            }
        } else if (event.data.msg == "loadWindow") {
            location.reload();
        } else if (event.data.msg == 'windowdimensions') {
            windowDimensions();
        } else if (event.data.msg == 'removeParentHash') {
            var uri = window.location.toString();
            if (uri.indexOf("#") > 0) {
                var clean_uri = uri.substring(0, uri.indexOf("#"));
                window.history.replaceState({}, document.title, clean_uri);
            }
        } else {
            let tempHeight;
            // if ((typeof window.screen.orientation != 'undefined' && window.screen.orientation.type == 'portrait-primary') || (typeof window.orientation != 'undefined' && (window.orientation == 0 || window.orientation == 180))) {
            //     tempHeight = window.screen.width;
            //     zino_iframe.style.height = parseInt(event.data) > tempHeight ? event.data + "px" : tempHeight + "px";
            // } else if (window.screen.width > 990 && ((typeof window.screen.orientation != 'undefined' && window.screen.orientation.type == 'landscape-primary') || (typeof window.orientation != 'undefined' && (window.orientation == 90 || window.orientation == -90)))) {
            //     tempHeight = 225;
            //     zino_iframe.style.height = parseInt(event.data) > tempHeight ? event.data + "px" : tempHeight + "px";
            // } else {
            //     zino_iframe.style.height = 100 + '%';
            // }
            if (window.screen.width < window.screen.height) {
                tempHeight = window.screen.width;
                zino_iframe.style.height = parseInt(event.data) > tempHeight ? event.data + "px" : tempHeight + "px";
            } else if (window.screen.width > 990 && window.screen.width >= window.screen.height) {
                tempHeight = 225;
                zino_iframe.style.height = parseInt(event.data) > tempHeight ? event.data + "px" : tempHeight + "px";
            } else {
                zino_iframe.style.height = 100 + '%';
            }
        }
    }
};

var orientationIframe = function (event) {
    var zino_iframe = document.getElementById(iframe_id);
    // let tempHeight;
    // if (window.screen.orientation.type == 'portrait-primary') {
    //     tempHeight = window.screen.width + 'px';
    // } else if (window.screen.width > 990 && window.screen.orientation.type == 'landscape-primary') {
    //     tempHeight = 225 + 'px';        
    // } else {
    //     tempHeight = 100 + '%';
    //     zino_iframe.style.height = tempHeight;
    // }
    var myObj = new Object();
    myObj.msg = 'orientationchange';
    myObj.orientation = window.orientation;
    zino_iframe.contentWindow.postMessage(myObj, "*");
};

function getPosition(el) {
    // var x = 0;
    // var y = 0;
    // while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
    // x += el.offsetLeft - el.scrollLeft;
    // y += el.offsetTop - el.scrollTop;
    // el = el.offsetParent;
    // }
    // return { top: y, left: x };
    return el.getBoundingClientRect();
}

function windowDimensions() {
    var zino_iframe = document.getElementById(iframe_id);
    var myObj = new Object();
    myObj.msg = 'windowdimensions';
    if (!(document.fullscreenElement || document.mozFullScreen || document.webkitIsFullScreen || document.msFullscreenElement)) {
        myObj.height = window.innerHeight - 40;
    } else {
        myObj.height = window.innerHeight;
    }
    myObj.width = window.innerWidth;
    zino_iframe.contentWindow.postMessage(myObj, "*");
    window.scrollTo({
        top: 40,
        behavior: 'smooth'
    });
}

if (window.addEventListener) {
    window.addEventListener("message", zino_resize, false);
    window.addEventListener("orientationchange", orientationIframe, false);
    window.addEventListener("resize", windowDimensions, false);
} else if (window.attachEvent) {
    window.attachEvent("onmessage", zino_resize);
    window.attachEvent("onorientationchange", orientationIframe);
    window.attachEvent("onresize", windowDimensions);
}
if (document.addEventListener) {
    document.addEventListener("fullscreenchange", windowDimensions, false);
    document.addEventListener("mozfullscreenchange", windowDimensions, false);
    document.addEventListener("webkitfullscreenchange", windowDimensions, false);
    document.addEventListener("msfullscreenchange", windowDimensions, false);
} else if (window.attachEvent) {
    document.attachEvent("onfullscreenchange", windowDimensions);
    document.attachEvent("onmozfullscreenchange", windowDimensions);
    document.attachEvent("onwebkitfullscreenchange", windowDimensions);
    document.attachEvent("onmsfullscreenchange", windowDimensions);
}